#!/usr/bin/env python3
# coding: utf-8
# encoding=utf8
#
# Copyright CEP(Kage Park)
# license : GPL
#   It can be sell by CEP Only.
#   Others according to GPL license(Only Freeware software).
# CEP Library for Python
# Since : March 2016
# Portting from shell scripts(1994 ~2016) to Python code.
# Help python module : help(<module name>)

# Version :<type>.<main>.<bug fix/addition in the main>.<testing in the bug/addition>
# <type> - 0:shell, 1: python, 2: C, 3:OSX, 4:iOS, 5:Android, 6:Java, 7:Fortran, 8:Unknown
_k_v_version="1.10.2.28" # it changed from 1.9.1.37

# common
import sys, os, time, shutil, stat, signal, re

# time
from datetime import datetime
from datetime import timedelta

# extra lib for daemon
#import argparse, logging, atexit

#OS specital
import platform,traceback,inspect
osname=platform.system()
import importlib,subprocess

# For network
import socket,ssl

import pickle,base64
# Queue for python2
if sys.version_info >= (3,0):
  from queue import Queue
else:
  import Queue

# Input
import select

# Data convert
import ast
import copy
import random,hashlib

#Return Code
K_ERROR={'ERR'}
K_FAIL={'FAL'}
K_FALSE=K_FAIL
K_TRUE={'TRU'}
K_OK=K_TRUE
K_ALREADY={'ALR'}
K_NOT_FOUND={'NFO'}
K_NO_INPUT={'NIN'}
K_WRONG_FORMAT={'WFO'}
K_UNKNOWN={'UNK'}

#Temperary DB in CEP
CEP_TMP={}
_kv_dbg=0

# Base command
# Internal command
def control_c():
      signal.signal(signal.SIGINT,exit)

def version():
    return _k_v_version

def help(obj):
    return var(obj,'help')
 
def now(detail=0,int_type=0):
    return CMD().now(detail=detail,int_type=int_type)

def isfile(filename):
    return CMD().is_file(filename)

def is_dir(path):
    return CMD().is_dir(path)

def log(msg=None,level=0,detail=False,filename=None):
    '''Logging or debugging'''
    log_msg='{0} : {1}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"),msg)
    if filename is not None:
       new_path=os.path.dirname(filename)
       if CODE().code_check(os.path.isdir(new_path),False):
           os.mkdir(new_path)
       f = open(filename,'a')
       f.write(log_msg + '\n')
       f.close()
       dbg(level=_kv_dbg,msg=msg,detail=False,filename=filename)
    else:
       print(log_msg)

def dbg(level=0,msg=None,detail=False,filename=None):
    """ debugging logging process """
    if _kv_dbg < level:
      return K_OK
    stack = traceback.extract_stack()
    frame = inspect.currentframe().f_back
    argvalues = inspect.getargvalues(frame)
    arg_str = inspect.formatargvalues(*argvalues)
    total=len(stack) - 1
    now = datetime.now()
    if detail:
      dmsg = '''{0}: + {1} at {2} in {3}, call {4} at {5} line'''.format(now.strftime("%H:%M:%S"),stack[0][3],stack[0][1],stack[0][0],stack[1][3],stack[1][1])
    else:
      dmsg = '''{0}: + {1}'''.format(now.strftime("%H:%M:%S"),stack[0][3])
    tap="          "
    for ii in range(2,total):
        for jj in range(1,ii):
            tap=tap+" "
        if detail:
            if ii == total-1:
                dmsg = '''\n{0}{1}\_. {2} {3} in {4}, call {5} at {6} line'''.format(dmsg,tap,stack[ii][2],arg_str,stack[ii][0],stack[ii][3],stack[ii][1])
            else:
                dmsg = '''\n{0}{1}\_. {2} in {3}, call {4} at {5} line'''.format(dmsg,tap,stack[ii][2],stack[ii][0],stack[ii][3],stack[ii][1])
        else:
            dmsg = '''\n{0}{1}\_. {2}'''.format(dmsg,tap,stack[ii][2])
    dmsg = '{0}\n{1} => {2}'.format(dmsg,tap,msg)
    if filename is not None:
        f = open('{0}.{1}'.format(filename,now.strftime("%Y-%m-%d")),'a')
        f.write(dmsg + '\n')
        f.close()
    else:
        print(dmsg)

def mk_path_type(obj,cnt=None,add=None):
      obj_type=type(obj).__name__
      tmp=''
      if obj_type == 'list':
         tmp=list2path(obj)
      elif obj_type == 'dict':
         tmp=list2path(dict2list_path(obj))
      else:
         tmp=obj
      if add is not None:
         tmp=tmp+'/'+add
      return tmp

def is_ver3():
      try:
         assert sys.version_info >= (3,0)
         return K_TRUE
      except AssertionError:
         return K_FALSE

def is_ver2():
      try:
         assert sys.version_info >= (2,0) and sys.version_info < (3,0)
         return K_TRUE
      except AssertionError:
         return K_FALSE

# Shell
def shell(cmd,stdout=-1,timeout=None,dbg=False):
    """ OS Shell command process in Python """
    if CODE().code_check(is_ver3(),K_TRUE):
        return shell3x(cmd,stdout=stdout,timeout=timeout,dbg=dbg)
    else:
        return shell2x(cmd,stdout=stdout,timeout=timeout,dbg=dbg)

def shell2x(cmd,stdout=-1,timeout=None,dbg=False):
    Popen=subprocess.Popen
    PIPE=subprocess.PIPE
    STDOUT=subprocess.STDOUT
    if dbg:
       p = Popen('set -x\n'+ cmd + '\n', shell=True, stdout=PIPE, stderr=STDOUT, executable='/bin/bash',bufsize=1)
    else:
       p = Popen(cmd, shell=True, stdout=PIPE, stderr=STDOUT, executable='/bin/bash',bufsize=1)

    if timeout is not None:
       def kill_proc():
          timer.expired = True
          p.kill()
       timer = Timer(timeout, kill_proc)
       timer.expired = False
       timer.start()

    if CODE().code_check(stdout,True):
       for line in iter(p.stdout.readline, b''):
          print(line.decode('utf-8').rstrip())
       p.stdout.close()
       p.wait()
    else:
       out, err = p.communicate()
       if p.returncode == 0 and p.returncode is not None:
          return out.rstrip()
    if p.returncode is None:
       return 1
    else:
       return p.returncode

def shell3x(cmd,stdout=-1,timeout=None):
    """ OS Shell command process in Python """
    Popen=subprocess.Popen
    PIPE=subprocess.PIPE
    p = Popen(cmd , shell=True, stdout=PIPE, stderr=PIPE)

    command='command is ' + cmd
    self.dbg(4,command)
    try:
      if timeout is None:
          out, err = p.communicate()
      else:
          out, err = p.communicate(timeout=timeout)
      if p.returncode == 0:
          if CODE().code_check(stdout,True):
              print(out.decode("utf-8").rstrip())
          return out.decode("utf-8").rstrip()
      else:
          if CODE().code_check(stdout,True):
              print("ERR("+p.returncode+") : " + err.decode("utf-8").rstrip())
          return K_ERROR
    except subprocess.TimeoutExpired:
      p.kill()
      if stdout == 1:
          print('Kill process after timeout ({0} sec)'.format(timeout))
      return K_ERROR

def rshell(cmd):
    Popen=subprocess.Popen
    PIPE=subprocess.PIPE
    p = Popen(cmd , shell=True, stdout=PIPE, stderr=PIPE)
    out, err = p.communicate()
    return p.returncode, out.decode("ISO-8859-1").rstrip(), err.decode("ISO-8859-1")

def get_django(django_url=None,url_port=8000,ids=None,list_tag=False,data=None):
    import requests
    if django_url is None:
        django_url='http://172.16.115.130:8000/list/'
    else:
        if url_port is None:
            django_url_arr=django_url.split(':')
            if len(django_url_arr) == 1:
                url_port=8000
            else:
                url_port=django_url_arr[1]
        django_url='http://{0}:{1}/list/'.format(django_url_arr[0],url_port)

    ss = requests.Session()
    rc={}
    if data is not None:
        rc=data
    if list_tag or ids == 'all':
        host_url='{0}'.format(django_url)
        try:
            r = ss.post(host_url, verify=False)
        except requests.exceptions.RequestException as e:
            return K_FALSE
        json_data=json.loads(r.text)
        if ids is None:
            return json_data

    if ids is None:
        return K_FALSE

    if ids == 'all':
        ids=''
        for ii in json_data:
           if ids == '' or ids == 'all':
               ids='{0}'.format(ii)
           else:
               ids='{0},{1}'.format(ids,ii)

    for ii in ids.split(','):
        if not ii in rc.keys():
            host_url='{0}{1}/'.format(django_url,ii)
            try:
                r = ss.post(host_url, verify=False)
            except requests.exceptions.RequestException as e:
                return K_FALSE
            try:
                json_data=json.loads(r.text)
            except:
                return K_FALSE
            rc[json_data['id']]={}
            rc[json_data['id']]={'code':json_data['code'],'crelay':json_data['crelay']}

    return rc

def post_url(url,user=None,password=None,options=None,token='csrftoken',session_file='/tmp/k.tmp',ssl=False):
    import requests
    '''
    ssl is not correctly not work
    '''
    ssl_file=False
    if ssl:
        #ssl_file=requests.certs.where()
        #ssl_file=certifi.old_where()
        ssl_file=False
    if user is not None and password is not None and os.path.isfile(session_file):
        os.unlink(session_file)
    if os.path.isfile(session_file):
        with open(session_file) as f:
            ss = pickle.load(f)
        csrf_token=ss.cookies[token]
        data={'csrfmiddlewaretoken':csrf_token}
        if options is not None and len(options) > 0:
            data.update(options)
        try:
            r = ss.post(url, data=data, verify=ssl_file) # ssl
        except:
            r = False
    else:
        # Login
        ss = requests.Session()
        ss.stream = False
        try:
            ss.get(url,verify=ssl_file) # ssl
        except requests.exceptions.RequestException as e:
            print('{0}\n{1}\n{2}'.format(url,ssl_file, e))
            return False

        csrf_token=ss.cookies[token]
        data={'csrfmiddlewaretoken':csrf_token,'username': user, 'password':password}
        if options is not None and len(options) > 0:
            data.update(options)
        headers={"X-CSRFToken": csrf_token}
        try:
            r = ss.post(url, data=data, headers=headers, verify=ssl_file) # ssl
            with open(session_file,'wb') as f:
                session = pickle.dump(ss,f)
        except:
            r = False
    if r.status_code == requests.codes.ok:
        return r
    else:
        return False

def get_os_class():
    """ Get OS dependance class
      k = cep.CEP()
      odc = k.get_os_class()
      odc.<function> or odc.<init variable name>
    
      odc will be OS dependanced Class
    """
    if 'Linux' == osname:
        if shell('''[ -f /etc/os-release ] && ( . /etc/os-release ; echo $ID)''') == 'ubuntu':
            return OS().UBUNTU()
        else:
            return OS().LINUX()
    elif 'Darwin' == osname:
        return OS().OSX()
    elif 'Windows' == osname:
        return OS().WINDOWS()
    else:
        return K_UNKNOWN

def self():
    # same as "sys.modules[__name__]" in local python script
    frm = inspect.stack()[1]
    mod = inspect.getmodule(frm[0])
    return mod

def export_path(paths):
    '''
    Append Python PATH as 'export PATH' command in SHELL
    usage) path('path') or path(['path1','path2',...])
    '''
    if paths is not None and paths != '':
       for path in append([],symbol=',',a=paths):
          if os.path.isdir(path):
             sys.path.append(path)
          else:
             print('** {0} path not found'.format(path))

def is_stream_input():
    # stream input example
    # for line in sys.stdin:
    #     print(line)
    if select.select([sys.stdin,],[],[],0.0)[0]:
        return True
    return False

#def put(data=None,form=None,symbol=None,force=False,safe=True,extract=False):
#    return DATA().put(data=data,form=form,symbol=symbol,force=force,safe=safe,extract=extract)
#
#def get(data=None,form=None,symbol=None):
#    return DATA().get(data=data,form=form,symbol=symbol)
#
#def append(src,**data):
#    return DATA().append(src,**data)

# TMP DB
def tmp_db(keys=None,val=None,cmd='add'):
   global CEP_TMP
   if cmd == 'clear':
      CEP_TMP={}
   if keys is None:
      return CEP_TMP
   tmp=CEP_TMP
   path_keys=path2list(keys)
   max_idx=len(path_keys)
   for idx, key in enumerate(path_keys):
      if idx == 0 and key == '':
         if val is None:
            return tmp
         else:
            if type(val).__name__ == 'dict':
               tmp.update(val)
            else:
               print('2')
               return K_FALSE
      else:
         if idx < max_idx -1 and type(tmp) is dict:
            if not key in tmp:
               if val is None:
                  return K_FALSE
               tmp[key]={}
            tmp=tmp[key]
         else: # End of key
            if type(tmp) is dict:
               if val is None:
                  if not key in tmp:
                     return K_FALSE
                  if cmd == 'del' or cmd == 'remove' or cmd == 'delete':
                      tmp.pop(key,None)
                  else:
                      return tmp[key]
               else:
                  if type(val) is dict:
                     tmp[key].update(val)
                  else:
                     tmp[key]=val
            else:
               print('1')
               return K_FALSE

# Text Code
def encoding(char='utf8'):
      reload(sys)
      sys.setdefaultencoding(char)

def convert_encoding(data=None, new_coding='UTF-8'):
      if data is None:
         return K_FALSE
      if CODE().code_check(imports('cchardet',globals()),False):
         return K_FALSE
      encoding = cchardet.detect(data)['encoding']
      if encoding is None:
         return K_FALSE
      if new_coding.upper() != encoding.upper():
         data = data.decode(encoding, data).encode(new_coding)
      return data

def convert_char(filename=None, new_coding='UTF-8'):
      if filename is None:
         return K_FALSE
      with open(filename,"rb") as f:
         msg = f.read()
      return convert_encoding(msg,new_coding)

# Exit / Error
def error_exit(msg=None):
    ''' Error Exit '''
    if msg is not None:
        print(msg)
    else:
        print('error')
    sys.exit(K_ERROR)

def exit(signal=None,frame=None,msg=None,code=None):
      if code == "no":
        return K_OK
      if msg is not None:
        print(msg)
      if code is None:
        sys.exit(0)
      else:
        sys.exit(code2shell(code))

#def code_check(code,check):
#    return CODE().code_check(code,check)
#
#def ptime(**data):
#    return TIME().ptime(**data)
#
#def check_date(**data):
#    return TIME().check_date(**data)
#
#def check_time(itime,during=None):
#    return TIME().check_time(itime,during=during)
#
#def time_check(start,end,check=None):
#    return TIME().time_check(start,end,check=check)

def var(obj,rctype=None,idx=None):
      """ Find type or convert value to want type
      var(type(<var name>),<var name>) => return type
      var(type(<var name>),<var name>,'<want type>') => return converted type value """
      if obj is None:
         return

      stype = type(obj).__name__
      if rctype is None:
         return stype

      if stype == rctype:
         return obj

      if stype == 'bytes':
         strobj = obj.decode('utf-8')
      elif stype == 'list':
         mx = len(obj)
         if idx is not None:
            strobj = None
            for ii in range(mx):
               if ii == idx:
                  strobj = obj[ii]
         else:
            strobj = ''
            mx = len(obj)
            for ii in range(mx):
               strobj = strobj + obj[ii]
               if mx > 1 and mx -1 != ii:
                  strobj = strobj + ' '
      elif stype == 'dict':
         strobj = ''
         mx = len(obj)
         for ii in list(obj):
            strobj = strobj + ii
            if mx > 1:
               strobj = strobj + ' '
      elif stype == 'dict_keys':
         strobj = ''
         mx = len(obj)
         for ii in list(obj):
            strobj = strobj + ii
            if mx > 1:
               strobj = strobj + ' '
      else:
         strobj = str(obj)

      if rctype == 'int':
         try:
            return int(strobj)
         except:
            return K_ERROR
      elif rctype == 'float':
         try:
            return float(strobj)
         except:
            return K_ERROR
      elif rctype == 'str' or rctype == 'string':
         try:
            return str(strobj)
         except:
            return K_ERROR
      elif rctype == 'bytes':
         try:
            return str.encode(strobj)
         except:
            return K_ERROR
      elif rctype == 'help':
         try:
            return obj.__doc__
         except:
            return
      else:
         print('Can not convert yet')
         return K_ERROR

def wopt(find,num=0,force=0,fixed=0):
      try:
         idx=sys.argv.index(find)
      except:
         idx=len(sys.argv)+2

      if fixed > 0:
         if idx != fixed:
            return

      mx=len(sys.argv)
      tmp=[]
      breaked=0
      for ii in range(1,mx):
         if ii < idx or ii > idx+num:
            tmp.append(sys.argv[ii])
         else:
            if ii == idx:
               continue
            if sys.argv[ii][0] == '-':
               breaked=1
            if breaked == 1:
               tmp.append(sys.argv[ii])
      return tmp

def load_shell_lib(db,shell_body,ignore_shell='k.k'):
        sql=SQLite()
        # Load required shell library
        req_body=[]
        ignore_shell_list=ignore_shell.split(',')
        for ii in list(shell_body.split('\n')):
                jj = ii.split(' ')
                if jj[0] == '#include':
                    if any(jj[1] in s for s in ignore_shell_list):
                        continue
                    req_lib=jj[1].split('.')[0]
                    num=sql.find(db,'shell','/'+req_lib)
                    if len(num) < 1 :
                        print(jj[1] + ' not found in shell DB')
                        sys.exit(-1)
                    else:
#                        print('Load ' + jj[1])
                        req_body.insert(0,sql.convert_get(sql.get(db,'shell','/'+req_lib)[0][0]) + '\n')

                        req_body.insert(1,ignore_shell+','+jj[1])
        return req_body

def _copystat(src=None,dest=None,st=None):
      if dest is None or (st is None and src is None):
         return
      if st is None:
         st = os.stat(src)
      if hasattr(os,'utime'):
         os.utime(dest,(st.st_atime, st.st_mtime))
      if hasattr(os,'chmod'):
         mode = stat.S_IMODE(st.st_mode)
         os.chmod(dest, mode)
      if hasattr(os,'chown'):
         os.chown(dest,st.st_uid,st.st_gid)

def opt(find,num=0,force=0,fixed=0):
      """ handle option """
      try:
         idx=sys.argv.index(find)
      except:
         return

      mx=len(sys.argv)
      chk=idx+num
      if num == 'max' or num == 'auto':
         chk=mx-1
      elif num == 0:
         return idx
      if fixed > 0:
         if idx != fixed:
            return
      tmp=[]

      if mx <= chk:
         chk=mx-1

      for ii in range(idx+1,chk+1):
         if force == 1:
            tmp.append(sys.argv[ii])
         else:
            if sys.argv[ii][0] == '-':
               break
            else:
               tmp.append(sys.argv[ii])
      return tmp

def q_init(d,qnum=1,db_file=None,force=False):
      if CODE().code_check(imports('Queue',globals()),False):
         return K_FALSE
      q=Queue.Queue(qnum)
      if db_file is None:
         db=d
      else:
         if CODE().code_check(is_file(db_file),True) and force is False:
             db=file2memory(db_file)
         else:
             db=d
             memory2file(db,db_file)

      q.put(db)
      return q

def q_get(q,keys=None):
      gd = q.get()
      d = gd.copy()
      q.put(gd)
      if keys is not None:
          return dict_get(d,keys=keys)
      else:
          return d

def q_update(q,d,keys=None,db_file=None):
      gd = q.get()
      try:
         gd.update(d)
      finally:
         q.put(gd)
         if db_file is not None:
            memory2file(gd,db_file)
         return K_OK
      return K_FALSE

def q_pop(q,keys,db_file=None):
      rc=False
      gd = q.get()
      if gd:
         gd=dict_del(gd,keys)
         rc=True
      q.put(gd)
      if db_file is not None:
         memory2file(gd,db_file)
      return rc

def q_clean(q,db_file=None):
      gd = q.get()
      q.put({})
      return K_OK

def memory2file(obj, fname,enc=True):
     if CODE().code_check(imports('pickle',globals()),False):
        return K_FALSE
     if enc:
        if CODE().code_check(imports('base64',globals()),False):
           return K_FALSE
     with open(fname, 'wb') as f:
        if enc:
           f.write(base64.b64encode(pickle.dumps(obj)))
        else:
           pickle.dump(obj, f, protocol=pickle.HIGHEST_PROTOCOL)

def file2memory(fname,enc=True):
     if is_file(fname) is False:
        print("%s not found" % fname)
        return

     if CODE().code_check(imports('pickle',globals()),False):
        return K_FALSE
     if enc:
        if CODE().code_check(imports('base64',globals()),False):
           return K_FALSE
     with open(fname, 'rb') as f:
        if enc:
           return pickle.loads(base64.b64decode(f.read()))
        else:
           return pickle.load(f)

# Time
class TIME:
    def ptime(self,**data):
        '''
        ** Print format time
        rf=xxxx : read format (default:auto detect) (ex: %Y-%m-%d %H:%M:%S, %Y-%m-%d %I:%M:%S%p)
        <add|del>=[<##>N1,<##>N2,...] or <##>N  # Add or del <##>y(year),w(week),d(day),h(hour),m(min),s(sec)
        p=<format> : print format(yy(year),mm(mon),dd(day),hh(hour),m(min),ss(sec),ts(total sec),ww(week number),wd(weekday),wn(weekday name),apm(am/pm), %Y-%m-%d %H:%M:%S(default format))
        default return is TIME
        '''
        _print_format='%Y-%m-%d %H:%M:%S'
        _read_format=None
        _time=datetime.now()
        if len(data) > 0:
            keys=data.keys()
            if 'pf' in keys: # print form
                _print_format=data['pf']
            elif 'format' in keys: # print form
                _print_format=data['format']
        
            if 'rf' in keys: # Read form
                _read_format=data['rf']
        
            if 't' in keys: # input time
                if _read_format is None:
                   time_date_format=None
                   time_time=None
                   time_apm=None
                   if type(data['t']) is list:
                       input_date=data['t'][0]
                   else:
                       input_date=data['t']
                   if type(input_date) is not str:
                       return False
                   #time_str_arr=data['t'].split(' ')
                   time_str_arr=input_date.split(' ')
        
                   # 1 or 2 value
                   #date
                   s_time_str_arr_1=time_str_arr[0].split('/')
                   d_time_str_arr_1=time_str_arr[0].split('-')
                   t_time_str_arr_1=time_str_arr[0].split(':')
                   if len(s_time_str_arr_1) > 1:
                       if len(s_time_str_arr_1) == 3:
                           if len(s_time_str_arr[0]) == 4:
                               time_date_format='%Y/%m/%d'
                           else:
                               time_date_format='%m/%d/%y'
                       elif len(s_time_str_arr_1) == 2:
                               time_date_format='%m/%d'
                   elif len(d_time_str_arr_1) > 1:
                       if len(d_time_str_arr_1) == 3:
                           if len(d_time_str_arr_1[0]) == 4:
                               time_date_format='%Y-%m-%d'
                           else:
                               time_date_format='%m-%d-%y'
                       elif len(d_time_str_arr_1) == 2:
                           time_date_format='%m-%d'
                   elif len(t_time_str_arr_1) > 1:
                   #time
                       if len(t_time_str_arr_1) == 3:
                           if len(time_str_arr) == 2:
                               _read_format='%I:%M:%S %p'
                           elif len(time_str_arr) == 1:
                               if time_str_arr[0][-1] == 'm' or time_str_arr[0][-1] == 'M':
                                   _read_format='%I:%M:%S%p'
                               else:
                                   _read_format='%H:%M:%S'
                       elif len(t_time_str_arr_1) == 2:
                           if len(time_str_arr) == 2:
                               _read_format='%I:%M %p'
                           elif len(time_str_arr) == 1:
                               if time_str_arr[0][-1] == 'm' or time_str_arr[0][-1] == 'M':
                                   _read_format='%I:%M%p'
                               else:
                                   _read_format='%H:%M'
        
                   # 2 or 3 value
                   #time
                   if time_date_format is not None and _read_format is None and len(time_str_arr) > 1:
                       t_time_str_arr_2=time_str_arr[1].split(':')
                       if len(t_time_str_arr_2) > 1:
                           if len(t_time_str_arr_2) == 3:
                               if len(time_str_arr) == 3:
                                   if time_str_arr[2][-1] == 'm' or time_str_arr[2][-1] == 'M':
                                       _read_format='{0} %I:%M:%S %p'.format(time_date_format)
                               else:
                                   if time_str_arr[1][-1] == 'm' or time_str_arr[1][-1] == 'M':
                                       _read_format='{0} %I:%M:%S%p'.format(time_date_format)
                                   else:
                                       _read_format='{0} %H:%M:%S'.format(time_date_format)
                           elif len(t_time_str_arr_2) == 2:
                               if len(time_str_arr) == 3:
                                   if time_str_arr[2][-1] == 'm' or time_str_arr[2][-1] == 'M':
                                       _read_format='{0} %I:%M %p'.format(time_date_format)
                               else:
                                   if time_str_arr[1][-1] == 'm' or time_str_arr[1][-1] == 'M':
                                       _read_format='{0} %I:%M%p'.format(time_date_format)
                                   else:
                                       _read_format='{0} %H:%M'.format(time_date_format)
        
                   if _read_format is None:
                       print("can't find format for {0}. please give time format".format(data['t']))
                       return False
                   _time=datetime.strptime(data['t'],_read_format)
        
            def get_timedelta(key='add'):
                _year=0
                _week=0
                _day=0
                _hour=0
                _min=0
                _sec=0
                if not key in data:
                    return (_day,_hour,_min,_sec)
        
                if type(data[key]) is list:
                    for _add in list(data[key]):
                        _add_symbol=_add[-1]
                        _new_add=int(_add[:-1])
                        if _add_symbol == 'y':
                            _year=_new_add
                        elif _add_symbol == 'w':
                            _week=_new_add
                        elif _add_symbol == 'd':
                            _day=_new_add
                        elif _add_symbol == 'h':
                            _hour=_new_add
                        elif _add_symbol == 'm':
                            _min=_new_add
                        elif _add_symbol == 's':
                            _sec=_new_add
                else:
                    keytype = type(data[key])
                    if keytype is int:
                        _min=data[key]
                    elif keytype is str:
                        _add_symbol=data[key][-1]
                        _new_add=int(data[key][:-1])
                        if _add_symbol == 'y':
                            _year=_new_add
                        elif _add_symbol == 'w':
                            _week=_new_add
                        elif _add_symbol == 'd':
                            _day=_new_add
                        elif _add_symbol == 'h':
                            _hour=_new_add
                        elif _add_symbol == 'm':
                            _min=_new_add
                        elif _add_symbol == 's':
                            _sec=_new_add
        
                if _year > 0:
                    _day=_day+(_year * 365)
                if _week > 0:
                    _day=_day+(_week * 7)
                return (_day,_hour,_min,_sec)
        
            if 'add' in keys: # Add time
                if data['add'] == 'today':
                    dn=datetime.now()
                    _time=_time.replace(year=dn.year,month=dn.month,day=dn.day)
                else:
                    _day,_hour,_min,_sec=get_timedelta(key='add')
                    _time = _time + timedelta(days=_day,hours=_hour,minutes=_min,seconds=_sec)
            elif 'del' in keys: # delete time
                _day,_hour,_min,_sec=get_timedelta(key='del')
                _time = _time - timedelta(days=_day,hours=_hour,minutes=_min,seconds=_sec)
        
            if 'p' in keys: # print short value
                _print=data['p']
                if _print == 'day' or _print == 'dd':
                    return _time.day
                elif _print == 'year' or _print == 'yy':
                    return _time.year
                elif _print == 'month' or _print == 'mon' and _print == 'mm':
                    return _time.month
                elif _print == 'hour' or _print == 'hh':
                    return _time.hour
                elif _print == 'minute' or _print == 'min' or _print == 'm':
                    return _time.minute
                elif _print == 'second' or _print == 'sec' or _print == 'ss':
                    return _time.minutes
                elif _print == 'week_number' or _print == 'ww':
                    return _time.isocalendar()[1]
                elif _print == 'weekday' or _print == 'wd':
                    return _time.weekday()
                elif _print == 'weekday_name' or _print == 'wn':
                    return _time.strftime('%a')
                elif _print == 'apm':
                    return _time.strftime('%p')
                elif _print == 'ts' or _print == 'total_sec':
                    return _time.strftime('%s')
                elif _print == 'now':
                    return datetime.now().strftime('%s')
                elif _print == 'today':
                    return datetime.now().date()
                elif _print == 'today2':
                    return datetime.now().strftime('%Y/%m/%d')
                else:
                    if len(data['p'].split('%')) > 1:
                        return _time.strftime(data['p'])# print format
                    else:
                        return _time.strftime(_print_format)# print format
        
        return _time # return to time

    def check_date(self,**data):
        year=None
        month=None
        day=None
        
        def compare(a,b,c):
            if type(a) is str:
                a=int(a)
            if type(b) is str:
                a=int(b)
            if c == 'lt':
                if a < b:
                    return True
            elif c == 'le':
                if a <= b:
                    return True
            elif c == 'gt':
                if a > b:
                    return True
            elif c == 'ge':
                if a >= b:
                    return True
            else:
                if a == b:
                    return True
            return False
        
        if 'd' in data:
            try:
                idate=ast.literal_eval(data['d'])
            except:
                idate=data['d']
            d_type=type(idate)
            if d_type is str:
                if 'df' in data:
                    date_date=datetime.strptime(data['d'],data['df'])
                    if date_date.year == 1900:
                        return {'year':None,'month':date_date.month,'day':date_date.day}
                    else:
                        return {'year':date_date.year,'month':date_date.month,'day':date_date.day}
                else:
                    ssplit=data['d'].split('/') 
                    if len(ssplit) == 1:
                        ssplit=data['d'].split('-') 
                    if len(ssplit) == 3:
                        try:
                            year=ast.literal_eval(ssplit[0])
                        except:
                            year=ssplit[0]
                        try:
                            month=ast.literal_eval(ssplit[1])
                        except:
                            month=ssplit[1]
                        try:
                            day=ast.literal_eval(ssplit[2])
                        except:
                            day=ssplit[2]
                    elif len(ssplit) == 2:
                        try:
                            month=ast.literal_eval(ssplit[0])
                        except:
                            month=ssplit[1]
                        try:
                            day=ast.literal_eval(ssplit[1])
                        except:
                            day=ssplit[1]
                    elif len(ssplit) == 1:
                        try:
                            day=ast.literal_eval(ssplit[0])
                        except:
                            day=ssplit[0]
                    return {'year':year,'month':month,'day':day}
            elif d_type == type([]):
                tmp=[]
                #for ii in list(data['d']):
                for ii in list(idate):
                    tmp.append(self.check_date(d=ii))
                return tmp
        
        if ('dd' in data or 'day' in data) and ('mm' in data or 'month' in data) and ('yy' in data or 'year' in data):
            if 'dd' in data:
                day=data['dd']
            else:
                day=data['day']
            if 'mm' in data:
                month=data['mm']
            else:
                month=data['month']
            if 'yy' in data:
                year=data['yy']
            else:
                year=data['year']
            return compare(datetime.now().date(),datetime.strptime('{0}-{1}-{2}'.format(year,month,day),'%Y-%m-%d').date(),data['compare']) 
        if ('dd' in data or 'day' in data) and ('mm' in data or 'month' in data):
            if 'dd' in data:
                day=data['dd']
            else:
                day=data['day']
            if 'mm' in data:
                month=data['mm']
            else:
                month=data['month']
            year=datetime.now().year
            return compare(datetime.now().date(),datetime.strptime('{0}-{1}-{2}'.format(year,month,day),'%Y-%m-%d').date(),data['compare'])
        
        if 'yy' in data or 'year' in data:
            now_year=datetime.now().year
            if 'yy' in data:
                year=data['yy']
            else:
                year=data['year']
        
            if year is None:
                return True
            if type(year) is str:
                    try:
                        year=ast.literal_eval(year)
                    except:
                        pass
            if type(year) is str:
                year_arr=year.split('-')
                if len(year_arr) == 1:
                    if 'compare' in data:
                        return compare(now_year,year,data['compare'])
                    else:
                        if now_year == int(year):
                            return True
                elif len(year_arr) == 2:
                    if int(year_arr[0]) <= now_year and now_year <= int(year_arr[1]):
                        return True
            elif type(year) is int:
                if 'compare' in data:
                    return compare(now_year,year,data['compare'])
                else:
                    if now_year == int(year):
                        return True
            elif type(year) is list:
                for sch_year in list(year):
                    if type(sch_year) == type([]):
                        if int(sch_year[0]) <= now_year and now_year <= int(sch_year[1]):
                            return True
                    else:
                        year_arr=sch_year.split('-')
                        if len(year_arr) == 1:
                            if now_year == int(sch_year):
                                return True
                        elif len(year_arr) == 2:
                            if int(year_arr[0]) <= now_year and now_year <= int(year_arr[1]):
                                return True
            return False
        
        if 'mm' in data or 'month' in data:
            now_month=datetime.now().month
            if 'mm' in data:
                month=data['mm']
            else:
                month=data['month']
            if month is None:
                return True
            if type(month) is str:
                    try:
                        month=ast.literal_eval(month)
                    except:
                        pass
            if type(month) is str:
                month_arr=month.split('-')
                if len(month_arr) == 1:
                    if 'compare' in data:
                        return compare(now_month,month,data['compare'])
                    else:
                        if now_month == int(month):
                            return True
                elif len(month_arr) == 2:
                    if int(month_arr[0]) <= now_month and now_month <= int(month_arr[1]):
                        return True
            elif type(month) is int:
                if 'compare' in data:
                    return compare(now_month,month,data['compare'])
                else:
                    if now_month == int(month):
                        return True
            elif type(month) is list:
                for sch_month in list(month):
                    if type(sch_month) == type([]):
                        if int(sch_month[0]) <= now_month and now_month <= int(sch_month[1]):
                            return True
                    else:
                        month_arr=sch_month.split('-')
                        if len(month_arr) == 1:
                            if now_month == int(sch_month):
                                return True
                        elif len(month_arr) == 2:
                            if int(month_arr[0]) <= now_month and now_month <= int(month_arr[1]):
                                return True
            return False
        
        if 'dd' in data or 'day' in data:
            now_day=datetime.now().day
            if 'dd' in data:
                day=data['dd']
            else:
                day=data['day']
        
            if day is None:
                return True
            if type(day) is str:
                    try:
                        day=ast.literal_eval(day)
                    except:
                        pass
            if type(day) is str:
                day_arr=day.split('-')
                if len(day_arr) == 1:
                    if 'compare' in data:
                        return compare(now_day,day,data['compare'])
                    else:
                        if now_day == int(day):
                            return True
                elif len(day_arr) == 2:
                    if int(day_arr[0]) <= now_day and now_day <= int(day_arr[1]):
                        return True
            elif type(day) is int:
                if 'compare' in data:
                    return compare(now_day,day,data['compare'])
                else:
                    if now_day == int(day):
                        return True
            elif type(day) is list:
                for sch_day in list(day):
                    if type(sch_day) == type([]):
                        if int(sch_day[0]) <= now_day and now_day <= int(sch_day[1]):
                            return True
                    else:
                        day_arr=sch_day.split('-')
                        if len(day_arr) == 1:
                            if now_day == int(sch_day):
                                return True
                        elif len(day_arr) == 2:
                            if int(day_arr[0]) <= now_day and now_day <= int(day_arr[1]):
                                return True
            return False


    def check_weeknum(self,db):
        if not 'weeknum' in db:
            db['weeknum']=[]
        
        if len(db['weeknum']) == 0:
            return True
        else:
            day_of_month = datetime.now().day
            week_number = (day_of_month - 1) // 7 + 1
            for sch_weeknum in list(db['weeknum']):
                if int(week_number) == int(sch_weeknum):
                    return True
            return False

    def check_weekday(self,db):
        if not 'weekday' in db:
            db['weekday']=[]
        
        if len(db['weekday']) == 0:
            return True
        else:
            now_weekday=datetime.now().weekday()
            for sch_weekday in list(db['weekday']):
                if int(now_weekday) == int(sch_weekday):
                    return True
            return False

    def time_check(self,start,end,check=None):
        """  Check Time
        Time format : 10:00PM ~ 08:00AM , 07:00AM
        return K_FALSE  : correct
        return K_OK  : Not correct
        return K_ERROR : Error"""
        
        if start is None:
          return K_NO_INPUT
        if end is None:
          return K_NO_INPUT
        
        aa=datetime.strptime(start, '%I:%M%p')
        start_time=aa.strftime('%H%M')
        aa=datetime.strptime(end, '%I:%M%p')
        end_time=aa.strftime('%H%M')
        if check is None:
          check_time=datetime.now().strftime("%H%M")
        else:
          aa=datetime.strptime(check, '%I:%M%p')
          check_time=aa.strftime('%H%M')
        
        if start_time < end_time:
          if ( start_time <= check_time ) and ( check_time < end_time ) :
               rc = 1
          else:
               rc = 0
        elif start_time > end_time:
          if ( start_time > check_time ) and ( end_time < check_time) :
               rc = 0
          else:
               rc = 1
        else:
          rc = -1
        return rc

    def check_time(self,itime,during=None):
        now=self.ptime()
        if type(itime) == type([]):
            for nowtime in list(itime):
                nowtime_type=type(nowtime)
                if nowtime_type == type([]):
                    if len(nowtime) == 2:
                        starttime=self.ptime(t=nowtime[0],add='today')
                        endtime=self.ptime(t=nowtime[1],add='today')
                        if starttime > endtime:
                            endtime=self.ptime(t=endtime.strftime('%Y-%m-%d %H:%M'),add='1d')
                        if starttime <= now and now < endtime:
                            return True
                    elif len(nowtime) == 1:
                        nowtime=nowtime[0]
                        nowtime_type=type(nowtime)
        
                if nowtime_type is str:
                    if during is None or during == 0 or during == 1:
                        start_time=self.ptime(t=nowtime,p='%H:%M')
                        now_time=self.ptime(p='%H:%M')
                        if  start_time == now_time:
                            return True
                    else:
                        starttime=self.ptime(t=nowtime,add='today')
                        endtime=self.ptime(t=starttime.strftime('%Y-%m-%d %H:%M'),add='{0}m'.format(during))
                        if starttime <= now and now < endtime:
                            return True
        elif type(itime) is str:
             if during is None or during == 0 or during == 1:
                 start_time=self.ptime(t=itime,p='%H:%M')
                 now_time=self.ptime(p='%H:%M')
                 if  start_time == now_time:
                     return True
             else:
                 starttime=self.ptime(t=itime,add='today')
                 endtime=self.ptime(t=starttime.strftime('%Y-%m-%d %H:%M'),add='{0}m'.format(during))
                 if starttime <= now and now < endtime:
                     return True
        return False

# Method or Class
class METHOD:
    def find_method_in_class(self,class_name, func_name):
        for tst in type(class_name).mro():
            if func_name in tst.__dict__:
                return tst

    def get_method_in_class(self,class_name):
        '''
        Get method name in the Class
        usage) find_method_in_class(<class name>)
        '''
        ret = dir(class_name)
        if hasattr(class_name,'__bases__'):
            for base in class_name.__bases__:
                ret = ret + self.get_method_in_class(base)
        return ret

    def is_method_in_class(self,class_name, func_name):
        '''
        Check method name in the Class
        usage) is_method_in_class(<class name>,'<func name>')
        '''
        if func_name in self.get_method_in_class(class_name):
            return K_OK
        return K_FALSE

    def is_have_module(self,mod):
        try:
           imp.find_module(mod)
           return K_OK
        except:
           return K_FAIL

    def setup_module(self,libs,module_path=None,module_file=None):
        for lib in list(METHOD().get_module_name(libs)):
            if CODE().code_check(self.is_have_module(lib),K_FAIL):
                pip_file=CMD().get_pip()
                if module_file is not None:
                     rc=os.system('{0} install {1}'.format(pip_file,module_file))
                else:
                     rc=os.system('{0} install {1}'.format(pip_file,lib))
        return rc

    def is_loaded_module(self,mod,gv=None):
        '''
        Check module is loaded or not
        usage) is_loaded_module('module',globals())
        '''
        if gv is None:
            gv=inspect.stack()[1][0].f_globals # New code for get globals() from caller
        gv_keys=gv.keys()
        mod_name=self.get_module_name(mod)
        for ii in mod_name:
            if not ii in gv_keys:
                return K_FALSE
        return K_OK

    def is_not_loaded_module(self,mod,gv=None):
        '''
        Check module is not loaded or not
        usage) is_loaded_module('module',globals())
        '''
        if CODE().code_check(self.is_loaded_module(mod,gv=gv),K_OK):
            return K_FALSE
        return K_OK

    def get_module_path(self,mod,gv=None):
        '''
        Get Module path
        usage) get_module_path('abc',globals())
        '''
        if gv is None:
           gv=inspect.stack()[1][0].f_globals # New code for get globals() from caller
        if CODE().code_check(self.is_loaded_module(mod,gv),True):
           return gv.get(mod).__file__
        return 

    def get_module_name(self,mods):
        rc=[]
        for mod in DATA().append2list(mods):
            if type(mod).__name__ == 'module':
                rc.append(mod.__name__)
            elif type(mod).__name__ == 'str':
                rc.append(mod)
        return rc

    def reload_module(self,mods,gv=None):
        '''
        usage) reload_module('abc',globals()) or unload_module(['abc','def',...],globals())
        '''
        if gv is None:
           gv=inspect.stack()[1][0].f_globals # New code for get globals() from caller
        for mod in list(self.get_module_name(mods)):
           if CODE().code_check(self.is_loaded_module(mod,gv),K_OK):
              sys.modules.pop(mod)
              gg[mod] = importlib.import_module(mod)
        return K_OK

    def unload_module(self,mods,gv=None):
        '''
        Unload loaded module list
        usage) unload_module('abc',globals()) or unload_module(['abc','def',...],globals())
        '''
        if gv is None:
           gv=inspect.stack()[1][0].f_globals # New code for get globals() from caller
        for mod in list(self.get_module_name(mods)):
           if CODE().code_check(self.is_loaded_module(mod,gv),K_OK):
              sys.modules.pop(mod)
        return K_OK

    def imports(self,libs,gg=None,gv=None,module_path=None,module_file=None,auto_install=True):
        '''
         Check module is loaded or not.
         if not loaded module then try load.
         if load fail then try install the module and try load again
         usage) imports('<mod name>',globals())" or "imports(['<mod1>','<mod2>',...],globals())
         option)
           module_path='<<mod name> path>' or ['<<mod1> path1>','<<mod2> path2>',...]
        
         usage2) imports('<mod name>',globals(),module_file='<module install file>')
        '''
        
        if gg is None and gv is None:
           gg=inspect.stack()[1][0].f_globals # New code for get globals() from caller
        else:
           if gg is None:
              if gv is not None:
                 gg=gv
              else:
                 print("""Please use "imports('<mod name>',globals())" or "imports(['<mod1>','<mod2>',...],globals())" """)
                 return K_NO_INPUT
        
        export_path(module_path)
        rc=''
        for lib in list(self.get_module_name(libs)):
           try:
              if CODE().code_check(self.is_not_loaded_module(lib,gg),True):
                 gg[lib] = importlib.import_module(lib)
           except ImportError:
              if auto_install:
                 rc=setup_module(lib,module_path=module_path,module_file=module_file)
                 if rc==0:
                    gg[lib] = importlib.import_module(lib)
                 elif rc==256:
                    return K_FALSE
                 else:
                    return K_UNKNOWN
              else: 
                 rc='{0}** MISSING Module: {1}\n'.format(rc,lib)
        if rc == '':
           return K_OK
        else:
           return rc

    def get_caller_module_name(self):
        frm = inspect.stack()[1]
        mod = inspect.getmodule(frm[0])
        return mod.__name__

    def get_caller_fcuntion_name(self,detail=False):
        try:
            if detail:
                return sys._getframe(2).f_code.co_name,sys._getframe(2).f_lineno,sys._getframe(2).f_code.co_filename
            else:
                return sys._getframe(2).f_code.co_name
        except:
            return False

    def get_function_list(self,obj=None):
        '''
        current module(python script) : __name__
        func_list=get_function_list(<obj>)
        func_list.keys(): function name string
        func_list[<key>]: function name object
        '''
        aa={}
        if obj is None:
            obj=__name__
        for name,obj in inspect.getmembers(sys.modules[obj]):
            if inspect.isfunction(obj): # inspect.ismodule(obj) check the obj is module or not
                aa.update({name:obj})
        return aa

    def get_function_name(self):
        return traceback.extract_stack(None, 2)[0][2]

    # Get Default parameter's value
    def get_function_args(self,func):
        """
        returns a dictionary of arg_name:default_values for the input function
        """
        args, varargs, keywords, defaults = inspect.getargspec(func)
        if defaults is not None:
            return dict(zip(args[-len(defaults):], defaults))

    def run_function_name_argv(self,obj,func_name,*arg): # Get variable inputs like as console input(argv)
        '''
        Run function name in object with variable input.
        but the function take fixed variable inputs
        use)run_function_name_arg(<class>,<func_name>[,'v1',...])
        ex)
        run_function_name_arg(abc,test,3,'c') : work
        run_function_name_arg(abc,test,3) : not work
        
        arg=tuple(sys.argv[2:])
        run_function_name_arg(abc,mm,*arg) : work
        
        class abc:
           def test(a=None,b=None):
             .......
           def mm(*arg):
             .......
        '''
        if CODE().code_check(self.is_method_in_class(obj, func_name),K_OK):
            if len(arg) == 0:
                return getattr(obj,func_name)()
            else:
                return getattr(obj,func_name)(*arg) # Extract tuple to variable 
        else:
            return K_ERROR

    def run_function_name(self,obj,func_name,**arg): # Get variable inputs with dictionary
        '''
        Run function name in object with variable input.
        but the function take variable inputs
        use)run_function_name(<class>,<func_name>[,a='v1',...])
        ex)
        run_function_name(abc,test,a=3,b='c') : work
        run_function_name(abc,test,a=3) : work
        
        class abc:
           def test(a=None,b=None):
             ....
        '''
        if CODE().code_check(self.is_method_in_class(obj, func_name),K_OK):
            return getattr(obj,func_name)(**arg) # Extract dict to variable
        else:
            return K_ERROR

class CMD:
    def __init__(self):
        self.size_table={'bb':{'u':'Byte','s':1},'kb':{'u':'KB','s':1024},'mb':{'u':'MB','s':1048576},'gb':{'u':'GB','s':1073741824},'tb':{'u':'TB','s':1099511627776}}

    def size_byte(self,size,unit):
        if unit in self.size_table.keys():
            return int(size*self.size_table[unit]['s'])
        
    def size_convert(self,byte_size,out_size='auto'):
        size_unit_list=['bb','kb','mb','gb','tb']
        if out_size in size_unit_list:
            idx=size_unit_list.index(out_size)
        else:
            idx=len(size_unit_list)-1
        for i in range(0,idx+1):
            if byte_size < self.size_table[size_unit_list[i]]['s']:
                return float('%3.2f'%((byte_size * 1.) / self.size_table[size_unit_list[i-1]]['s'])),self.size_table[size_unit_list[i-1]]['u']
        return float('%3.2f'%((byte_size * 1.)/ self.size_table[size_unit_list[i]]['s'])),self.size_table[size_unit_list[i]]['u']

    def get_pip(self,pip_name=None,full_path=False):
        def cpip(ver3,pip_name=[],full_path=False):
            if ver3 == K_TRUE:
                pip_list=pip_name+['pip3','pip3.6']
            else:
                pip_list=pip_name+['pip2','pip2.7','pip2.6']
            for pip_file in pip_list:
                if os.path.exists('/usr/bin/{0}'.format(pip_file)):
                    if full_path:
                        return '/usr/bin/{0}'.format(pip_file)
                    return pip_file
        ver3=is_ver3()
        if type(pip_name) is str:
            pip_name=pip_name.split(',')
        elif type(pip_name) is list:
            pass
        else:
            pip_name=[]
        pip_file=cpip(ver3,pip_name)
        if pip_file is not None:
            return pip_file
        self.download_url_file('https://bootstrap.pypa.io/get-pip.py','/tmp/get-pip.py')
        os.system('python -o /tmp/get-pip.py')
        pip_file=cpip(ver3,pip_name)
        if pip_file is not None:
            return pip_file

    def progress_percents(self,msg,percent):
        status = "%s: [%3.2f%%]\r" %(msg,percent)
        sys.stdout.write(status)
        sys.stdout.flush()

    def progress(self,pp=0):
        pps=['|','/','-','\\','|']
        sys.stdout.write(pps[pp] + '\r')
        sys.stdout.flush()
        if pp >= 4:
           pp=0
        else:
           pp=pp+1
        return pp

    def download_url_file(self,url,save):
        if url is None or save is None:
            return False
        data,file_size,ctype=NET().url(url)
        file_size_d,file_size_u=self.size_convert(file_size)
        file_name = url.split('/')[-1]
        file_size_s=0
        block_size=8192
        f = open(save, 'wb')
        while True:
            bsize = data.read(block_size)
            if not bsize:
                break
            file_size_s += len(bsize)
            f.write(bsize)
            msg="Downloading: %s (%s %s)"%(file_name,file_size_d,file_size_u)
            perc=file_size_s * 100. /file_size
            self.progress_percents(msg,perc)
        f.close()
        print('')
        print('Saved at {0}'.format(save))

    def pkg(self):
        os_name = get_os_name()
        if os_name == 'UBUNTU':
            if len(sys.argv) < 1:
                print('pkg [ -repo <yum repo> or -iso <iso file> ] <command> <package name>')
                print('pkg install pip : install pip')
                self.k.exit()
        
            if sys.argv[0] == 'list' or sys.argv[0] == '-qa':
                if len(sys.argv[1:]) == 0:
                    os.system('dpkg -l')
                else:
                    os.system('dpkg -l '+ self.k.str(sys.argv[1:]))
            elif sys.argv[0] == 'install':
                if sys.argv[1] == 'pip':
                    if os.system('pip -V >& /dev/null') != 0:
                        os.system('sudo apt-get install python-setuptools; sudo python -m easy_install pip')
                    else:
                        print('Installed')
                elif sys.argv[1] == 'pip3':
                    if os.system('pip3 -V >& /dev/null') != 0:
                        os.system('apt-get install python3-pip')
                    else:
                        print('Installed')
                else:
                    os.system('sudo apt-get install  '+ self.k.str(sys.argv[1:]))
            elif sys.argv[0] == '-qf':
                os.system('dpkg -S '+self.k.str(sys.argv[1:]))
            elif sys.argv[0] == '-ql':
                os.system('dpkg -L '+self.k.str(sys.argv[1:]))
            elif sys.argv[0] == '-qi':
                os.system('apt-cache show '+self.k.str(sys.argv[1:]))
            else:
                os.system('dpkg '+ self.k.str(sys.argv))
        elif os_name == 'LINUX':
            if len(sys.argv) < 1:
                    print('pkg [ -repo <yum repo> or -iso <iso file> ] <command> <package name>')
                    print('pkg install pip : install pip')
                    self.k.exit()
            iso_mnt=None
            my_repo=''
            my_repo_file=None
            if sys.argv[0] == '-iso':
                iso_mnt = self.mount(sys.argv[1])
                if iso_mnt is None:
                    print(sys.argv[1] + ' is not iso file')
                    self.k.exit()
                sys.argv.pop(0)
                sys.argv.pop(0)
                my_repo_file = self.repo(iso_mnt)
                my_repo = '-c ' + my_repo_file + ' '
            elif sys.argv[0] == '-repo':
                my_repo='-c sys.argv[1] '
                sys.argv.pop(0)
                sys.argv.pop(0)
            if sys.argv[0] == 'list':
                os.system('yum '+ my_repo + self.k.str(sys.argv))
            elif sys.argv[0] == '-qa':
                os.system('rpm -qa '+ self.k.str(sys.argv))
            elif sys.argv[0] == 'info' or sys.argv[0] == '-qi':
                if is_file(self.k.str(sys.argv[1:])) is True:
                    os.system('rpm -qip '+ self.k.str(sys.argv[1:]))
                else:
                    os.system('yum '+ my_repo + self.k.str(sys.argv) + ' | grep "^From repo"; rpm -qi '+ self.k.str(sys.argv[1:]))
            elif sys.argv[0] == 'file' or sys.argv[0] == '-ql':
                if is_file(self.k.str(sys.argv[1:])) is True:
                    os.system('rpm -qlp '+ self.k.str(sys.argv[1:]))
                else:
                    os.system('rpm -ql '+ self.k.str(sys.argv[1:]))
            elif sys.argv[0] == 'find' or sys.argv[0] == 'search' or sys.argv[0] == '-qf':
                if is_file(self.k.str(sys.argv[1:])) is True:
                    os.system('rpm -qf ' + self.k.str(sys.argv[1:]))
                else:
                    os.system('yum ' + my_repo + ' search '+ self.k.str(sys.argv[1:]))
            elif sys.argv[0] == 'install' or sys.argv[0] == '-ihv':
                if len(sys.argv) < 2:
                    print('pkg [ -repo <yum repo> or -iso <iso file> ] install <package name>')
                    print('pkg install pip : install pip')
                    self.k.exit()
                if sys.argv[1] == 'pip':
                    if os.system('pip -V >& /dev/null') != 0:
                        os.system('''cd /tmp;  curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py" ; python get-pip.py''')
                    else:
                        print('Installed')
                elif sys.argv[1] == 'pip3':
                    if os.system('pip3 -V >& /dev/null') != 0:
                        os.system('''cd /tmp;  curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py" ; python3 get-pip.py''')
                    else:
                        print('Installed')
                else:
                    if is_file(my_repo_file) is True:
                        os.system('yum ' + my_repo + ' install --nogpgcheck --noplugins '+self.k.str(sys.argv[1:]))
                    else:
                        os.system('yum ' + ' install --nogpgcheck --noplugins --disablerepo=* '+self.k.str(sys.argv[1:]))
            else:
                os.system('yum '+ my_repo + self.k.str(sys.argv))
            if iso_mnt is not None:
                os.system('umount ' + iso_mnt)
                os.rmdir(iso_mnt)
                if is_file(my_repo_file) is True:
                    os.remove(my_repo_file)
        elif os_name == 'OSX':
            if len(sys.argv) < 1:
                print('pkg [ -repo <yum repo> or -iso <iso file> ] <command> <package name>')
                print('pkg install pip : install pip')
                os.system('brew --help')
                self.k.exit()
            if sys.argv[0] == 'list':
                os.system('brew list')
            elif sys.argv[0] == 'find':
                sys.argv.pop(0)
                os.system('brew search '+self.k.str(sys.argv))
            elif sys.argv[0] == 'file':
                sys.argv.pop(0)
                os.system('brew list '+ self.k.str(sys.argv))
            elif sys.argv[0] == 'install':
                if sys.argv[1] == 'pip':
                    if os.system('pip -V >& /dev/null') != 0:
                        os.system('''brew home >& /dev/null || ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"''')
                        os.system('brew update')
                        os.system('brew list python >& /dev/null || brew install python')
                        os.system('easy_install --version >& /dev/null || curl https://bootstrap.pypa.io/ez_setup.py -o - | sudo python')
                        os.system('sudo easy_install pip')
                    else:
                        print('Installed')
                elif sys.argv[1] == 'pip3':
                    if os.system('pip3 -V >& /dev/null') != 0:
                        os.system('''brew home >& /dev/null || ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"''')
                        os.system('brew update')
                        os.system('brew list python3 >& /dev/null || brew install python3')
                        os.system('easy_install --version >& /dev/null || curl https://bootstrap.pypa.io/ez_setup.py -o - | sudo python3')
                        os.system('sudo easy_install pip3')
                    else:
                        print('Installed')
                else:
                    sys.argv.pop(0)
                    os.system('brew install '+ self.k.str(sys.argv))
            else:
                os.system('brew '+ self.k.str(sys.argv))

    def create_snapfile(self,img_file,snap_file):
        if self.is_filetype(snap_file,'LVM Snapshot') > 0:
            return K_FALSE
        else:
            rc=shell('dd if=/dev/zero of='+snap_file+' bs=1 seek=$(($(stat -c %s '+img_file+')-4)) count=4 > /dev/null 2>&1 && return 0 || return -1')
            return rc

    def dm_find(self,name):
        rc=shell('dmsetup info '+name + ' >/dev/null 2>&1')
        if rc < 0 :
            return
        if os.path.islink('/dev/mapper/'+name) is True:
            return '/dev/mapper/'+name
        else:
            return

    def check_loop_free(self):
#        used_loop_num=shell('losetup -a | wc -l')
#        loop_num=shell('ls /dev/loop* | wc -l')
#        if int(loop_num) <= int(used_loop_num):
#            print('No more free loop devices')
#            self.k.exit()
        loop_dev=self.get_loop_dev()
        if not loop_dev or loop_dev is None or 'free' in loop_dev:
            return False
        if len(loop_dev['free']) == 0:
            print('no more free loop device')
            return
        dev=loop_dev['free']

    def unload_device(self,device=None):
        if dev is None:
            return
        dm_dev=DEV().dmsetup_info()
        if device in dm_dev.keys():
            if 'map' in dm_dev[device]:
                self.umount(dm_dev[device]['map'].keys()[0])
            shell('''dmsetup remove ''' + device)
            for ii in dm_dev[device]['map'].keys():
                for jj in dm_dev[device]['map'][ii]:
                    DEV().losetup(dev=jj) 
        

    def umount(self,dest=None,option='',unload_dev=False):
        aa=self.mountpoint(dest)
        if aa[0]:
           os.system('''umount ''' + aa[1])
        if unload_dev:
           self.unload_device(aa[2])

    def mountpoint(self,dest=None): # Looks same as os.path.ismount()
        if dest is not None and os.path.exists(dest):
           if os.path.isdir(dest):
               chk=1
           elif stat.S_ISBLK(os.stat(dest).st_mode):
               chk=0
           with open('/proc/mounts','r') as f:
               mm=f.read()
               for ii in mm.split('\n'):
                   aa=ii.split(' ')
                   if dest == aa[chk]:
                       return (True,aa[1],aa[0])
        return (False,None,None)

    def mount(self,source,dest=None,option=''):
        if source is None: return None
        if source.find(':') > 0:
            src_ar=source.split(':')
            source=self.load_device(src_ar[0],src_ar[1])

        aa=self.mountpoint(source)
        if aa[0]:
            print('{0}({1}) already mounted at {2}'.format(source,aa[2],aa[1]))
            self.k.exit()

        if dest is None:
            dest=self.mktemp('/tmp/tmp_mount.')
            if os.path.exists(dest) is False:
                os.makedirs(dest)
#        if os.path.ismount(dest) is True:
#            print(dest + ' directory is already mounted by something')
#            self.k.exit()

        if self.is_filetype(source,'ISO') > 0:
            os.system('mount -o loop ' + source + ' ' + dest)
        elif self.is_filetype(source,'ext3 filesystem') > 0:
            os.system('mount -o rw ' + source + ' ' + dest)
        elif self.is_filetype(source,'LVM Snapshot') > 0:
            print('give a <imagefile>:<snaphotfile> to source')
            self.k.exit()
        else:
            os.system('mount ' + option +' '+ source + ' ' + dest)
        return dest

    def repo(self,baseurl,os='rhel',gpgcheck=0,gpgkey=None):
        if baseurl is None: return None
        my_repo=self.mktemp('/tmp/yum_repo.')
        fo=open(my_repo,'w+')
        if os == 'rhel':
            fo.write('[myrepo]\n')
            fo.write('name=myrepo\n')
            fo.write('baseurl=file://'+baseurl+'\n')

        fo.write('enable=1\n')
        if gpgkey is None:
            fo.write('gpgcheck=0\n')
        else:
            fo.write('gpgcheck='+gpgcheck+'\n')
            fo.write('gpgkey=file://'+gpgkey)
        fo.close()
        return my_repo

    def fp_db_del(self, kfpdb=None,idx=None):
       if kfpdb is None or idx is None:
           return None

       fp_tbname='fp'
       del_rec="delete from {0} where idx={1}".format(fp_tbname,idx)
       kfpdel = kfpdb.cursor()
       kfpdel.execute(del_rec)
       kfpdb.commit()

    def fp_db_update(self, kfpdb=None,md5=None, fstat=None, idx=None,fname=None):
       if kfpdb is None or idx is None or fstat is None:
           return None
       if md5 is None and fname is None:
           return None

       fp_tbname='fp'
       if md5 is None:
           md5=self.md5(fname)
       fp_msg = "update {0} set md5='{1}',dev_id={2},inode={3},f_size={4} ,f_ctime={5} where idx={6}".format(fp_tbname, md5, fstat.st_dev, fstat.st_ino, fstat.st_size, fstat.st_ctime, idx)
       kfpupdate = kfpdb.cursor()
       kfpupdate.execute(fp_msg)
       kfpdb.commit()

    def fp_db_add(self, kfpdb=None,file_name=None,md5=None,fstat=None):
       if file_name is None:
          return K_FALSE

       fp_tbname='fp'
       os_name=platform.system()
       hostname=os.uname()[1]
       try:
          f_type=magic.from_file(file_name, mime=True)
       except IOError:
          f_type='unknown'

       if md5 is None:
          md5=self.md5(file_name)

       fp_msg = "insert into {0} (idx,md5,os_name,hostname,dev_id,inode,n_hlnk,f_size,f_ctime,f_type,f_path) values({1},'{2}','{3}','{4}',{5},{6},{7},{8},{9},'{10}','{11}')".format(fp_tbname, self.now(1), md5, os_name, hostname, fstat.st_dev, fstat.st_ino, fstat.st_nlink, fstat.st_size, fstat.st_ctime, f_type, file_name.replace("'","[\"]"))
       kfpadd = kfpdb.cursor()
       kfpadd.execute(fp_msg)
       kfpdb.commit()

    def fp_db_find(self, kfpdb=None,file_name=None,md5=None,ftype=None,fsize=None, bgfsize=None, ltfsize=None, fstat=None, idx=None,inode=None,dev_id=None, all_find=None, output="idx,md5,dev_id,inode,f_size,f_path"):
       if kfpdb is None:
           return None

       fp_tbname='fp'
       os_name=platform.system()
       hostname=os.uname()[1]
       find_db="select {0} from {1} where ".format(output,fp_tbname)
       sub_find_db=None
       if all_find is None:
          sub_find_db=" os_name='{0}' and hostname='{1}' ".format(os_name, hostname)

       if idx is not None:
          if sub_find_db is None:
             sub_find_db=" idx={0} ".format(idx)
          else:
             sub_find_db=sub_find_db+" and idx={0} ".format(idx)
       else:
          if fstat is not None:
             if sub_find_db is None:
                sub_find_db=" dev_id={0} and inode={1} and f_size={2} ".format(fstat.st_dev,fstat.st_ino,fstat.st_size)
             else:
                sub_find_db=sub_find_db+" and dev_id={0} and inode={1} and f_size={2} ".format(fstat.st_dev,fstat.st_ino,fstat.st_size)
          if inode is not None:
             if sub_find_db is None:
                sub_find_db=" inode={0} ".format(inode)
             else:
                sub_find_db=sub_find_db+" and inode={0} ".format(inode)
          if dev_id is not None:
             if sub_find_db is None:
                sub_find_db=" dev_id={0} ".format(dev_id)
             else:
                sub_find_db=sub_find_db+" and dev_id={0} ".format(dev_id)
          if md5 is not None:
             if sub_find_db is None:
                sub_find_db=" md5='{0}' ".format(md5)
             else:
                sub_find_db=sub_find_db+" and md5='{0}' ".format(md5)
          if ftype is not None:
             if sub_find_db is None:
                sub_find_db=" ftype='{0}' ".format(ftype)
             else:
                sub_find_db=sub_find_db+" and ftype='{0}' ".format(ftype)
          if fsize is not None:
             if sub_find_db is None:
                sub_find_db=" f_size={0} ".format(fsize)
             else:
                sub_find_db=sub_find_db+" and f_size={0} ".format(fsize)
          if bgfsize is not None:
             if sub_find_db is None:
                sub_find_db=" f_size>={0} ".format(bgfsize)
             else:
                sub_find_db=sub_find_db+" and f_size>={0} ".format(bgfsize)
          if ltfsize is not None:
             if sub_find_db is None:
                sub_find_db=" f_size<={0} ".format(ltfsize)
             else:
                sub_find_db=sub_find_db+" and f_size<={0} ".format(ltfsize)
          if file_name is not None:
             if sub_find_db is None:
                sub_find_db=" f_path='{0}' ".format(file_name.replace("'","[\"]"))
             else:
                sub_find_db=sub_find_db+" and f_path='{0}' ".format(file_name.replace("'","[\"]"))

       kfp_find_items = kfpdb.cursor()
       kfp_find_items.execute(find_db+sub_find_db)
       return kfp_find_items.fetchall()

    def fp(self, kfpdb=None,cmd=None,file_name=None,md5=None,ftype=None,fsize=None, lib=None, idx=None, chk=None, find_str=None):
       if kfpdb is None:
           return None

       if CODE().code_check(imports('magic',globals(),module_file='python-magic'),False):
           return K_FALSE
       fp_tbname='fp'
       # SQLite3 DB
       # idx float primary key not NULL, md5 text, os_name text, hostname text, dev_id int, inode int, n_hlnk int, f_size int, f_ctime int, f_type text, f_path text
    
       # State information
       # st_dev : device id
       # st_ino : file inode
       # st_size : file size
       # st_ctime : create time
       # st_nlink : number of hard links
    
       ###################################
       # Find duplicated fingerprint for all hosts
       if cmd == 'fdfp_all' or cmd == 'find_all':
          if md5 is not None:
             found_db=([md5,''],)
          elif file_name is not None:
             found_db=self.fp_db_find(kfpdb=kfpdb,fstat=os.stat(file_name),output="distinct md5",all_find=1)
          else:
             if ftype is not None:
                find_md5="select distinct md5 from {0} where f_type='{1}'".format(fp_tbname,ftype)
             elif fsize is not None:
                find_md5="select distinct md5 from {0} where f_size>={1}".format(fp_tbname,fsize)
             elif idx is not None:
                find_md5="select distinct md5 from {0} where idx={1}".format(fp_tbname,idx)
             elif find_str is not None:
                find_md5="select distinct md5 from {0} where f_path like '%{1}%'".format(fp_tbname,find_str)
             else:
                find_md5="select distinct md5 from {0}".format(fp_tbname)
             #print(find_md5)
             kfpfoundmd5 = kfpdb.cursor()
             kfpfoundmd5.execute(find_md5)
             found_db=kfpfoundmd5.fetchall()

          for tbmd5 in list(found_db):
              found_file=self.fp_db_find(kfpdb=kfpdb,md5=tbmd5[0],output="hostname,f_path",all_find=1)
              if lib == 1:
                 return len(found_file)
              if len(found_file) > 0 :
                  print(tbmd5[0]),
                  print(' : '),
                  print(file_name)
                  for ffname in list(found_file):
                      print(' + ' + ffname[0] + ' : ' + ffname[1].replace("[\"]","'"))
    
       ###################################
       # Find duplicated fingerprint for my host only
       elif cmd == 'fdfp' or cmd == 'find':
          if md5 is not None:
             found_db=([md5,''],)
          elif file_name is not None:
             found_db=self.fp_db_find(kfpdb=kfpdb,fstat=os.stat(file_name),output="distinct md5")
          else:
             if ftype is not None:
                find_md5="select distinct md5 from {0} where hostname='{1}' and f_type='{2}'".format(fp_tbname,os.uname()[1],ftype)
             elif fsize is not None:
                find_md5="select distinct md5 from {0} where hostname='{1}' and f_size>={2}".format(fp_tbname,os.uname()[1],fsize)
             elif idx is not None:
                find_md5="select distinct md5 from {0} where idx={1}".format(fp_tbname,idx)
             elif find_str is not None:
                find_md5="select distinct md5 from {0} where f_path like '%{1}%'".format(fp_tbname,find_str)
             else:
                find_md5="select distinct md5 from {0} where hostname='{1}'".format(fp_tbname,os.uname()[1])
          #print(find_md5)
             kfpfoundmd5 = kfpdb.cursor()
             kfpfoundmd5.execute(find_md5)
             found_db=kfpfoundmd5.fetchall()

          for tbmd5 in list(found_db):
              found_file=self.fp_db_find(kfpdb=kfpdb,md5=tbmd5[0],output="hostname,f_path")
              #print(found_file)
              if lib == 1:
                 return len(found_file)

              if len(found_file) > 0 :
                  print(tbmd5[0]),
                  print(' : '),
                  print(file_name)
                  for ffname in list(found_file):
                      print(' + ' + ffname[0] + ' : ' + ffname[1].replace("[\"]","'"))


    
       ###################################
       # cleanup not found files db
       elif cmd == 'clean':
          kfpfound = kfpdb.cursor()
          if ftype is not None:
             find_dbc="select idx from {0} where hostname='{1}' and f_type='{2}'".format(fp_tbname,os.uname()[1],ftype)
          elif fsize is not None:
             find_dbc="select idx from {0} where hostname='{1}' and f_size>={2}".format(fp_tbname,os.uname()[1],fsize)
          elif idx is not None:
             find_dbc="select idx from {0} where idx={1}".format(fp_tbname,idx)
          elif file_name is not None:
             find_dbc="select idx from {0} where hostname='{1}' and f_path like '%{2}%'".format(fp_tbname,os.uname()[1],file_name)
          else:
             find_dbc="select idx from {0} where hostname='{1}'".format(fp_tbname,os.uname()[1])
    
          for idxc in list(kfpfound.execute(find_dbc)):
              find_file="select f_path from {0} where idx={1}".format(fp_tbname,idxc[0])
              kfpfound.execute(find_file)
              found_filec=kfpfound.fetchone()
              if is_file(found_filec[0].replace("[\"]","'")) is False:
                  del_rec="delete from {0} where idx={1}".format(fp_tbname,idxc[0])
                  kfpfound.execute(del_rec)
                  kfpdb.commit()
                  print('clean db for ' + found_filec[0].replace("[\"]","'"))

          #kfpfound.execute("VACUUM")
       ###################################
       # Flush DB
       elif cmd == 'flush':
          flush = kfpdb.cursor()
          flush.execute("VACUUM")
    
       ###################################
       # cleanup not found files and update wrong DB
       elif cmd == 'update':
          kfpfound = kfpdb.cursor()
          if ftype is not None:
             find_dbc="select idx,md5,dev_id,inode,f_size,f_path from {0} where hostname='{1}' and f_type='{2}'".format(fp_tbname,os.uname()[1],ftype)
          elif idx is not None:
             find_dbc="select idx,md5,dev_id,inode,f_size,f_path from {0} where idx={1}".format(fp_tbname,idx)
          elif fsize is not None:
             find_dbc="select idx,md5,dev_id,inode,f_size,f_path from {0} where hostname='{1}' and f_size>={2}".format(fp_tbname,os.uname()[1],fsize)
          elif file_name is not None:
             find_dbc="select idx,md5,dev_id,inode,f_size,f_path from {0} where hostname='{1}' and f_path like '%{2}%'".format(fp_tbname,os.uname()[1],file_name)
          else:
             find_dbc="select idx,md5,dev_id,inode,f_size,f_path from {0} where hostname='{1}'".format(fp_tbname,os.uname()[1])
          deleted=0
          for idxc in list(kfpfound.execute(find_dbc)):
              ffc=idxc[5].replace("[\"]","'")
              if is_file(ffc) is False:
                  del_rec="delete from {0} where idx={1}".format(fp_tbname,idxc[0])
                  kfpfound.execute(del_rec)
                  kfpdb.commit()
                  print('cleanup db: ' + ffc)
              else:
                  fstat=os.stat(ffc)
                  file_path=os.path.abspath(ffc)

                  sim_path=self.fp_db_find(kfpdb=kfpdb,file_name=file_path)
                  sim_path_num=len(sim_path)
                  same_files=self.fp_db_find(kfpdb=kfpdb,file_name=file_path,fstat=fstat)
                  if sim_path_num == 1 and len(same_files) == 0: # similar file is one
                     # update the item
                     #self.fp_db_update(kfpdb=kfpdb,md5=md5, fstat=fstat, idx=sim_path[0][0])
                     self.fp_db_update(kfpdb=kfpdb,fstat=fstat, idx=sim_path[0][0],fname=file_path)
                     print('update db: ' + ffc)
                  elif sim_path_num > 1: # got many similar information
                     for del_other in list(sim_path):
                         if same_files[0][0] != del_other[0]:
                            del_rec="delete from {0} where idx={1}".format(fp_tbname,del_other[0])
                            kfpfound.execute(del_rec)
                            kfpdb.commit()
                            deleted=1
                            print('cleanup db: ' + ffc + ' at idx ' + str(del_other[0]))
                  elif sim_path_num == 0:
                      #self.fp_db_add(kfpdb=kfpdb,file_name=file_path,md5=md5,fstat=fstat)
                      self.fp_db_add(kfpdb=kfpdb,file_name=file_path,fstat=fstat)

       ###################################
       # delete a row in DB
       elif cmd == 'rm':
          if idx is not None:
             fnd_db="select count(idx) from {0} where idx={1}".format(fp_tbname,idx)
             rm_db="delete from {0} where idx={1}".format(fp_tbname,idx)
          else:
             kfpcur = kfpdb.cursor()
             os_name=platform.system()
             hostname=os.uname()[1]
             file_path=os.path.abspath(file_name)
             fnd_db="select count(idx) from {0} where os_name='{1}' and hostname='{2}' and f_path='{3}'".format(fp_tbname,os_name, hostname, file_path)
             rm_db="delete from {0} where os_name='{1}' and hostname='{2}' and f_path='{3}'".format(fp_tbname,os_name, hostname, file_path)
          kfpcur.execute(fnd_db)
          if kfpcur.fetchone()[0] > 0:
             kfpcur.execute(rm_db)
             kfpdb.commit()
             print('rm db: ' + file_name)
    
       ###################################
       # put informatioin to DB
       elif cmd == 'put':
          if file_name is None:
             return K_FALSE
          if is_file(file_name) is False:
             return K_FALSE
    
          file_path=os.path.abspath(file_name)
          fstat=os.stat(file_path)
          if chk == 'md5':
             fmd5=self.md5(file_path)
             found_db=self.fp_db_find(kfpdb=kfpdb,md5=fmd5)
          else:
             found_db=self.fp_db_find(kfpdb=kfpdb,fstat=fstat)

          if len(found_db) == 0:
                self.fp_db_add(kfpdb=kfpdb,file_name=file_path,fstat=fstat)
                return K_OK
          else:
                return K_FALSE
    
       else:
           print('Unknown command : ' + cmd)
           return K_FALSE

    def split_different_char(self,loc_str,ignore_char=None):
        tmp=''
        tmp2=''
        new_tmp=[]
        for ii in list(loc_str):
            if ii == ignore_char:
                continue
            if ii.isalpha() is True:
                if tmp2 == '':
                    tmp=tmp+ii
                else:
                    new_tmp.append(tmp2)
                    tmp2=''
                    tmp=ii
            elif ii.isalpha() is False:
                if tmp == '':
                    tmp2=tmp2+ii
                else:
                    new_tmp.append(tmp)
                    tmp=''
                    tmp2=ii
        new_tmp.append(tmp2)
        return new_tmp

    def get_os_name(self):
        if 'Linux' == osname:
            if shell('''[ -f /etc/os-release ] && ( . /etc/os-release ; echo $ID)''') == 'ubuntu':
                return 'UBUNTU'
            else:
                return 'LINUX'
        elif 'Darwin' == osname:
            return 'OSX'
        elif 'Windows' == osname:
            return 'WINDOWS'
        else:
            return K_UNKNOWN

    def is_running(self,app):
        """ Check running Apps in OSX """
        os_name=self.get_os_name()
        if os_name == 'OSX':
           try:
             count = int(subprocess.check_output(["osascript",
                    "-e", "tell application \"System Events\"",
                    "-e", "count (every process whose name is \"" + self.str(app) + "\")",
                    "-e", "end tell"]).strip())
             return count > 0
           except:
             return False
        return False

    def run_app(self,app):
        os_name=self.get_os_name()
        if os_name == 'OSX':
            napp=self.str(app)
            ab=self.osx_is_running(napp)
            if ab is None:
                return
            if ab is False:
                cmd="""osascript<<END
tell application "%s"
    activate
end tell
END""" % (napp)
                aa=self.shell(cmd)
                return aa
            else:
                return K_OK

    def sms_send(self,to,msg):
        os_name=self.get_os_name()
        if os_name == 'OSX':
            nmsg=str(msg)
            nto=str(to)
            cmd = """osascript<<END
tell application "Messages"
set targetService to 1st service whose service type = iMessage
set targetBuddy to buddy "%s" of targetService
send "%s" to targetBuddy
end tell
END""" % (nto, nmsg)
            dbg(3,cmd)
            aa=shell(cmd)
            return aa

    def sms_read(self,receive_account=None,sender_account=None,self_sender_ignore=True,imessage_db="{0}/Library/Messages/chat.db".format(os.path.expanduser('~')),read_time=0, find_text=None):
        os_name=self.get_os_name()
        if os_name == 'OSX':
            if CODE().code_check(imports('sqlite3',globals()),False):
                return K_NOT_FOUND
            imconn=sqlite3.connect(imessage_db)
            imlg=imconn.cursor()
            imessage_get="SELECT h.id, m.text, m.date FROM message m JOIN handle h ON m.handle_id=h.ROWID"
            receive_account_str=''
            sender_account_list=''
            self_sender_ignore_str=''
            block_recive_account=''
            set_mode={}
            wait_time=10 # 10min
            if receive_account != "all" and receive_account is not None:
                imessage_get=imessage_get+" and m.account='e:{0}' and h.id!='{1}'".format(receive_account,receive_account)

            if sender_account is not None:
                for ii in list(sender_account.split(',')):
                    if sender_account_list == '':
                        sender_account_list = "h.id='{0}'".format(ii)
                    else:
                        sender_account_list = "{0} or h.id='{1}'".format(sender_account_list,ii)
                imessage_get = imessage_get + " and ( {0} ) ".format(sender_account_list)

            if self_sender_ignore:
                imessage_get = imessage_get + ' and m.is_from_me=0'
            if find_text is not None:
                imessage_get = imessage_get + " and m.text LIKE '%{0}%'".format(find_text)

            if read_time > 0:
                imessage_get = imessage_get + " and m.date > %s" % (read_time)
            imlg.execute(imessage_get)
            imessage_fetchall=imlg.fetchall()
            imessage_last=len(imessage_fetchall)
            if imessage_last == 0:
                return
            return imessage_fetchall

    def cfg(self,file):
        from configobj import ConfigObj
        cfg = ConfigObj(file)
        return cfg

    def source(self,file,dict=None,raw=0):
        """ config example:
            [section1]
            var1 = data1
            var2 = data2
            var3 = %(var1)s
        
            [section2]
            var1: abc
            #var1: ${section1:var1} ${section1:var2}
        
            [section3]
            var1: 1111
            var2: 222
            #var3: ${var1} test
        
            obj.sections()                       : show list of sections
            obj.get('section1','var1')           : read value of var
            obj.remove_option('section1','var1') : remove var
            obj.add_section('section4')          : add new section
            obj.set('section4','var3','value')   : add/change data to var
            obj.write(config_file)               : write to file
        """
        if is_ver3() == K_TRUE:
            from configparser import SafeConfigParser
        else:
            from ConfigParser import SafeConfigParser
        config = SafeConfigParser()
        config.read(file)
        if dict == 1:
            new_dict = {}
            for section in config.sections():
               #print('sec=>'+self.str(section))
               if not section in new_dict:
                  new_dict[section] = {}
               for option in config.options(section):
                  if raw == 1:
                      new_dict[section][option] = config.get(section, option, raw=True)
                  else:
                      new_dict[section][option] = config.get(section, option)
        
            return new_dict
        else:
            return config

    def cut_string(self,string,len1=None,len2=None):
        if type(string).__name__ != 'str':
           string='{0}'.format(string)
        str_len=len(string)
        
        if len1 is None or len1 >= str_len:
           return [string]
        
        if len2 is None:
            rc=[string[i:i + len1] for i in range(0, str_len, len1)]
            return rc
        rc=[]
        rc.append(string[0:len1])
        string_tmp=string[len1:]
        string_tmp_len=len(string_tmp)
        for i in range(0, int(string_tmp_len/len2)+1):
            if (i+1)*len2 > string_tmp_len:
               rc.append(string_tmp[len2*i:])
            else:
               rc.append(string_tmp[len2*i:(i+1)*len2])
        return rc

    def chk_newline(self,fp_file=None):
        if fp_file is None:
           return K_ERROR
        if is_file(fp_file) is not True:
            return K_OK
        if os.stat(fp_file).st_size == 0:
            return K_OK
        f = open(fp_file,'r')
        last_line = f.readlines()[-1]
        f.close()
        if '\n' in last_line:
            return K_OK
        else:
            return K_FALSE

    def version_up(self,version=None):
        if version is None:
           return K_NO_INPUT
        new_ver=''
        version_arr=version.split('.')
        version_num=len(version_arr)
        edver=int(version_arr[version_num-1])+1
        if version_num == 1:
           new_ver=str(edver)
        else:
           new_ver=str(version_arr[0])
           for i in range(1,version_num-1):
             new_ver=new_ver+'.'+str(version_arr[i])
           new_ver=new_ver+'.'+str(edver)
        return new_ver

    def email_send(self,to,subject,msg):
          """ send a text to email """
          os_name=self.get_os_name()
          if CODE().code_check(os_name,'LINUX'):
             if html:
                 email_msg='''To: {0}
Subject: {1}
Content-Type: text/html
<html>
<body>
<pre>
{2}
</pre>
</body>
</html>'''.format(to,subject,msg)
                 cmd='''echo "{0}" | sendmail -t'''.format(email_msg)
             else:
                 if sender is None:
                    cmd='''echo -e "Subject: {0}\n{1}" | sendmail -t "{2}"'''.format(subject,msg,to)
                 else:
                    cmd='''echo -e "Subject: {0}\n{1}" | sendmail -f "{2}" -t "{3}"'''.format(subject,msg,sender,to)
          elif CODE().code_check(os_name,'OSX'):
              cmd = """osascript<<END
tell application "Mail"
  set theEmail to make new outgoing message with properties {visible:true, subject:"%s", content:"%s"}
  tell theEmail
    make new recipient at end of to recipients with properties {address:"%s"}
    send theEmail
  end tell
end tell
END""" % (subject, msg, to) 
          return shell(cmd)

    def is_sleep(self):
        os_name=self.get_os_name()
        if os_name == 'OSX':
            cmd = """ osascript<<END
tell application "System Events"
  get running of screen saver preferences
end tell
END"""
            sleep=shell(cmd)
            cmd = """ ioreg -n IODisplayWrangler |grep -i IOPowerManagement | sed -e "s/\\"//g" -e "s/\\}//g" | tr ',' '\\n' |grep CurrentPowerState | awk -F= '{print $2}' """
            screenpower=shell(cmd)
            ssave=self.is_running('ScreenSaverEngine')
            print(sleep,screenpower,ssave)
            dbg(4,'sleep os:' + var(sleep,'str') + ', screen power:' + var(screenpower,'str') + ', screen saver:' + var(ssave,'str'))

            if var(sleep,'str') == 'false' and var(screenpower,'str') == '4' and var(ssave,'str') == 'False':
                return K_FALSE
            else:
                return K_OK

    def tail(self,filename, n=10, loop=False):
        n=int(n)
        if is_file(filename) is False:
           return K_FALSE
        
        f = open(filename,'r')
        assert n >= 0
        pos, lines = n+1, []
        while len(lines) <= n:
          try:
                  f.seek(-pos, 2)
          except IOError:
                  f.seek(0)
                  break
          finally:
                  #lines = list(f)
                  #lines = f.tell()
                  lines=f.readlines()
                  #lines = f.read()
          pos *= 2
        f.close()
        shell_cmd=basename(sys.argv[0])
        aa=[]
        for ii in lines[-n:]:
            if shell_cmd == 'k':
                sys.stdout.write(ii)
            else:
                aa.append(ii)
        
        if shell_cmd != 'k':
            return CODE().code_convert(aa)

    def genstr(self,max=8,keys='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'):
        """ Make a temporary string """
        return ''.join(random.choice(keys) for i in range(max))

    def mktemp(self,filename=None,suffix=None,max=8,opt='dry',uniq=0):
        if filename is None:
           filename="/tmp/{0}".format(self.genstr(max))
        directory=os.path.dirname(filename)
        if directory == '.':
            directory=os.path.realpath(__file__)
        elif directory == '':
            directory='/tmp'
        
        pfilename, file_ext = os.path.splitext(filename)
        ifilename=os.path.basename(pfilename)
        if suffix is not None:
            file_ext='.{0}'.format(suffix)
        chk_X=ifilename.split('-')
        XXX=chk_X[len(chk_X)-1]
        if len(XXX.replace('X','')) == 0:
            max=len(XXX)
            new_XXX=str(self.genstr(max))
            ifilename=ifilename.replace('-{0}'.format(XXX),'-{0}'.format(new_XXX))
        
        dest_file='{0}/{1}{2}'.format(directory,ifilename,file_ext)
        for ii in range(10):
            if is_file(dest_file) is True or self.is_dir(dest_file) is True:
                dest_file='{0}/{1}-{2}{3}'.format(directory,ifilename,str(self.genstr(max)),file_ext)
        
        if opt == 'file':
           os.mknod(dest_file)
        elif opt == 'dir':
           os.mkdir(dest_file)
        return dest_file

    def now(self,detail=0,int_type=0):
        if detail == 1:
            if int_type == 1:
               aa="%d"%(time.time() * 100)
               return int(aa)
            elif int_type == 2:
               aa="%13.2f"%(time.time())
               return float(aa)
            else:
               return time.time()
        else:
          return int(datetime.now().strftime('%s'))

    def sleep(self,sec):
        if type(sec).__name__ == 'int':
           time.sleep(int(sec))
        return K_ERROR

    def md5(self,filename):
        if is_file(filename) is False:
           return K_FALSE
        try:
           # No memory issue for big file
           hash=hashlib.md5()
           with open(filename,'rb') as f:
               for chunk in iter(lambda: f.read(1048576),""):
                   hash.update(chunk)
           return hash.hexdigest()
        except IOError as e:
           print(e)
           return K_FALSE

    def convert_datetime2sec(self,date_str,format="%Y-%m-%d %H:%M:%S"):
        # -1 : time format error
        # -2 : time calculation error
        aa=date_str.split(' ')
        bb=format.split(' ')
        if len(aa) != len(bb):
           return K_WRONG_FORMAT
        try:
        #    return int(datetime.strptime(date_str,format).strftime('%s'))
            d=datetime.strptime(date_str,format)
            epoch=datetime.utcfromtimestamp(0)
            delta=d-epoch
            return int(delta.total_seconds())
        except:
            return K_ERROR

    def convert_sec2datetime(self,sec,format="%Y-%m-%d %H:%M:%S"):
        return time.strftime(format,time.gmtime(sec))

    def readlink(self,file=None,opt=None):
        if opt == '-p': # Read file full path
           if file is None:
              return os.path.abspath(__file__)
           else:
              return os.path.abspath(file)
        elif opt == 'dir' or opt == '-d': # read file directory
           if file is None:
              return os.path.dirname(os.path.abspath(__file__))
           else:
              return os.path.dirname(os.path.abspath(file))
        else: # read real file full path
           if file is None:
              return os.path.realpath(__file__)
           else:
              return os.path.realpath(file)

    def basename(self,path):
        return os.path.basename(path)

    def dirname(self,path):
        return os.path.dirname(path)

    def is_file(self,filename):
        if filename is None:
            return
        return os.path.isfile(filename)

    def is_dir(self,path):
        return os.path.isdir(path)

    def is_int(self,s):
        stype=type(s).__name__
        if stype == 'int':
           return K_OK
        elif stype == 'str':
           if len(s) > 0 and s[0] in ('-','+'):
              return s[1:].isdigit()
           return s.isdigit()
        else:
           return K_FALSE

    def is_filetype(self,filename,type=None):
        if CODE().code_check(imports('magic',globals(),module_file='python-magic'),False):
              return K_FALSE
        ftype=magic.from_file(filename)
        if type is None:
            return ftype
        else:
            return ftype.find(type)

    def compile(self,file,opt=None):
        if self.is_file(file) < 0:
           print(file + ' not found')
           return K_ERROR
        if opt == '-m' or opt == '--module':
           py_compile.compile(file,cfile=file+'c',doraise=True)
        else:
           if opt == '-w' or opt == '--window':
               rc=shell('pyinstaller --onefile --windowed '+file, stdout=True)
           else:
               rc=shell('pyinstaller --onefile '+file, stdout=True)
           if CODE().shell2code(rc) == K_OK:
               print('compiled binary file is in ./dist directory')
           else:
               print('build fail')

    def braket(self,src_str):
        if CODE().code_check(imports('re',globals()),False):
           return K_FALSE
        if src_str is None:
           return
        rc=[]
        src_str_arr=re.split(r'[\[\]]',src_str)
        if len(src_str_arr) == 1:
           for ii in list(src_str_arr[0].split(',')):
              rc.append(ii)
           return rc
        
        for ii in list(src_str_arr[1].split(',')):
           ii_rg=ii.split('-')
           ii_rg_len=len(ii_rg[0].replace(' ',''))
           pfm='%0{d}d'.format(d=ii_rg_len)
           if len(ii_rg) == 1:
              host=pfm%(int(ii_rg[0]))
              rc.append(src_str_arr[0]+host+src_str_arr[2])
           else:
              if ii_rg_len != len(ii_rg[1]):
                 print('mismatched string length')
                 return
              for jj in range(int(ii_rg[0]),int(ii_rg[1])+1):
                 host=pfm%(int(jj))
                 rc.append(src_str_arr[0]+host+src_str_arr[2])
        return rc

    def cp_file(self,src,dest,mode='auto',kfpdb=None):
      if os.path.exists(dest) is True:
          dest_dev_id=os.stat(dest)
      else:
          dest_dev_id=os.stat(os.path.dirname(os.path.abspath(dest)))

      if mode == 'copy':
         try:
            shutil.copy2(src,dest)
         except IOError as e:
            print('Unable to copy file. %s'%e)
            return K_FALSE
      else:
         src_dev_id=os.stat(src)
         if src_dev_id.st_dev == dest_dev_id.st_dev: # if same device id
            if os.path.isdir(dest) is True:
                os.link(src, dest+'/'+os.path.basename(src))
                self.fp(kfpdb=kfpdb,cmd='put',file_name=dest+'/'+os.path.basename(src))
            else:
                os.link(src, dest)
                self.fp(kfpdb=kfpdb,cmd='put',file_name=dest)
         else:
            rc=self.cp_file(src,dest,mode='copy')
            if rc is True:
                self.fp(kfpdb=kfpdb,cmd='put',file_name=dest)
            else:
                return rc
      return K_OK

    def cp(self,src,dest,mode='auto',kfpdb=None):
        if os.getcwd() != os.path.dirname(os.path.abspath(dest)):
           if os.path.isdir(os.path.dirname(os.path.abspath(dest))) is False:
              print('''Not found ''' + os.path.dirname(dest) + '''(destination's parent) ''' + ''' from '''+ dest)
              sys.exit(-1)
        
        copy2dir=0
        if os.path.isdir(dest) is True:
           copy2dir=1
        for src_item in list(src):
           if os.path.abspath(src_item) == os.path.abspath(dest):
               continue
           elif os.path.isdir(src_item) is True:
               src_arr=src_item.split('/')
               for dirname, dirnames, filenames in os.walk(src_item):
                   dir_arr=dirname.split('/')
                   new_dest=dest
                   if copy2dir == 1:
                       for ii in list(dir_arr[len(src_arr)-1:]):
                           new_dest=os.path.join(new_dest,ii)
                   else:
                       for ii in list(dir_arr[len(src_arr):]):
                           new_dest=os.path.join(new_dest,ii)
        
                   if os.path.isdir(new_dest) is False:
                       os.mkdir(new_dest)
                       _copystat(dirname,new_dest)
                   for filename in filenames:
                       if self.cp_file(os.path.join(dirname,filename),os.path.join(new_dest,filename),mode=mode,kfpdb=kfpdb) is False:
                           return K_FALSE
           else:
               if self.cp_file(src_item,dest,mode=mode,kfpdb=kfpdb) is False:
                    return K_FALSE
        return K_OK

    def rm(self,dest,force=None,kfpdb=None):
        for dest_item in list(dest):
            if os.path.isdir(dest_item) is True:
                if force == 1 or force == 3:
                    for dirname, dirnames, filenames in os.walk(dest_item):
                        # Need delete from end of depth files # kg code update
                        for filename in filenames:
                            new_dest=os.path.join(dirname,filename)
                            if os.unlink(new_dest) is None:
                                CEP().fp(kfpdb=kfpdb,cmd='rm',file_name=new_dest)
                        try:
                            os.rmdir(dirname)
                        except OSError as e:
                            continue
                    if os.path.isdir(dest_item) is True:
                        shutil.rmtree(dest_item)
                else:
                    return K_FALSE
            else:
                if self.is_file(dest_item) is True:
                    if os.unlink(dest_item) is None:
                        CEP().fp(kfpdb=kfpdb,cmd='rm',file_name=dest_item)
                    else:
                        return K_FALSE
                else:
                    return K_FALSE
        
        if kfpdb is not None and force == 3:
            kfpcur = kfpdb.cursor()
            kfpcur.execute("VACUUM")

    def mv(self,src,dest,kfpdb=None):
         # using cp command and correctly copy then unlink the source
         if cp(src,dest,kfpdb=kfpdb) is True:
             rm(src,kfpdb=kfpdb,force=1)

    def hex2int(self,hex_val):
         return eval("0x" + hex_val.replace(':','').lower())

    def int2hex(self,int_val):
         return hex(int_val)

    def hex2bit(self,hex_val,bits=16):
         scale=16
         return bin(int(hex_val,scale))[2:].zfill(bits)

    def bit2hex(self,bit):
         inttmp=int(str(bit),2)
         return "%X"%inttmp

    def get_ctime(self,filename):
        if CODE().code_check(imports(['struct','exifread'],globals()),False):
          return K_FALSE
        extension = os.path.splitext(filename)[1][1:].lower()
        if extension == 'mov' or extension == 'mp4':
           ATOM_HEADER_SIZE = 8
           # difference between Unix epoch and QuickTime epoch, in seconds
           EPOCH_ADJUSTER = 2082844800
           # open file and search for moov item
           f = open(filename, "rb")
           while 1:
              atom_header = f.read(ATOM_HEADER_SIZE)
              if atom_header[4:8] == 'moov':
                 break
              else:
                 atom_size = struct.unpack(">I", atom_header[0:4])[0]
                 f.seek(atom_size - 8, 1)
        
           # found 'moov', look for 'mvhd' and timestamps
           atom_header = f.read(ATOM_HEADER_SIZE)
           if atom_header[4:8] == 'cmov':
              print("moov atom is compressed")
           elif atom_header[4:8] != 'mvhd':
              print("expected to find 'mvhd' header")
           else:
              f.seek(4, 1)
              creation_date = struct.unpack(">I", f.read(4))[0]
              modification_date = struct.unpack(">I", f.read(4))[0]
              return datetime.utcfromtimestamp(creation_date - EPOCH_ADJUSTER)
        else:
           with open("%s" % filename, 'rb') as image:
             exif = exifread.process_file(image)
             if len(exif) == 0:
                if osname == 'Windows':
                   return os.path.getctime(filename)
                else:
                   stat = os.stat(filename)
                   try:
                      dt=stat.st_birthtime
                      return convert_sec2datetime(int(dt))
                   except AttributeError:
                      dt=stat.st_mtime
                      return convert_sec2datetime(int(dt))
             dt = str(exif['EXIF DateTimeOriginal'])
             return datetime.strptime(dt, '%Y:%m:%d %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')

    def ksort(self,string=None,new_line='\n',field_id=None,field_symbol='space',reverse=False,print_field=None,num=False):
        if string is None:
            return False
        
        sort_arr=[]
        line_arr=string.split(new_line)
        if field_id is None or field_symbol is None:
            if reverse:
                if num:
                   return sorted(line_arr,reverse=True,key=float)
                else:
                   return sorted(line_arr,reverse=True)
            else:
                if num:
                   return sorted(line_arr,key=float)
                else:
                   return sorted(line_arr)
        else:
            sort_dict={}
            for ii in list(line_arr):
                if ii == '':
                    continue
                if field_symbol == 'space':
                    ii_tmp=ii.split()
                else:
                    ii_tmp=ii.split(field_symbol)
                if len(ii_tmp) < field_id-1:
                   print('ignore field_id({0}) at "{1}" string'.format(field_id,ii))
                else:
                   if print_field is None:
                       sort_dict[ii_tmp.pop(field_id)]=ii
                   else:
                       tmp=''
                       for ss in print_field.split(','):
                           if len(ii_tmp) < int(ss)-1:
                               print('out of range print_field({0})'.format(ss))
                           else:
                               if tmp == '':
                                   tmp='{0}'.format(ii_tmp[int(ss)])
                               else:
                                   if field_symbol == 'space':
                                       tmp='{0} {1}'.format(tmp,ii_tmp[int(ss)])
                                   else:
                                       tmp='{0}{1}{2}'.format(tmp,field_symbol,ii_tmp[int(ss)])
                       sort_dict[ii_tmp.pop(field_id)]=tmp
            if reverse:
                if num:
                    sort_dict_keys=sorted(sort_dict.keys(),reverse=True,key=float)
                else:
                    sort_dict_keys=sorted(sort_dict.keys(),reverse=True)
            else:
                if num:
                    sort_dict_keys=sorted(sort_dict.keys(),key=float)
                else:
                    sort_dict_keys=sorted(sort_dict.keys())
            for jj in sort_dict_keys:
                sort_arr.append(sort_dict[jj])
            return sort_arr


    def wget(self,url, filename=None):
        if filename is not None:
            url='{0}/{1}'.format(url,filename)
        else:
            wget_url=url.split('/')
            filename=wget_url[len(wget_url)-1]
        try:
            print('download: {0}'.format(url))
            r=requests.get(url,stream=True)
        except requests.exceptions.RequestException as e:
            return K_FALSE
        pp=0
        with open(filename,'wb') as f:
            for chunk in r.iter_content(chunk_size=1024):
                pp=self.progress(pp=pp)
                f.write(chunk)
                f.flush()

    def kernel_version(self):
        os_name=os_name()
        if CODE().code_check(os_name,'OSX'):
            return platform.release()
        elif CODE().code_check(os_name,'LINUX'):
            return platform.release()
        elif CODE().code_check(os_name,'IOS'):
            return platform.release()

    def os_name(self):
        return get_os_name()

    def os_release(self):
        os_name=get_os_name()
        if CODE().code_check(os_name,'OSX'):
            return platform.mac_ver()[0]
        elif CODE().code_check(os_name,'LINUX'):
            return platform.dist()[1]
        elif CODE().code_check(os_name,'IOS'):
            return platform.mac_ver()[0]

    def lsmod(self,find=None):
        os_name=get_os_name()
        if CODE().code_check(os_name,'OSX'):
            return os.system('kextstat')
        elif CODE().code_check(os_name,'LINUX'):
            if find is None or len(find) ==0:
                rc=shell('lsmod',stdout=True)
            else:
                rc=shell('lsmod | grep {0}'.format(find[0]))
            return CODE().code_convert(rc)


    def insmod(self,module,opt=None):
        os_name=get_os_name()
        if CODE().code_check(os_name,'OSX'):
            return shell('sudo kextload -b ' + self.k.str(module))
        elif CODE().code_check(os_name,'LINUX'):
            if module is None or len(module) ==0:
                return K_ERROR
            else:
                for mm in list(module):
                   rc=K_FAIL
                   if opt is None:
                      rc=shell('insmod {0}'.format(mm))
                   if CODE().code_check(rc,K_FAIL) or CODE().code_check(rc,K_ERROR):
                      if opt is None:
                         rc=shell('modprobe {0}'.format(mm))
                      else:
                         rc=shell('modprobe {0} {1}'.format(mm,opt))
                      if CODE().code_check(rc,K_FAIL) or CODE().code_check(rc,K_ERROR):
                         return rc
            return rc

    def rmmod(self,module):
        os_name=get_os_name()
        if CODE().code_check(os_name,'OSX'):
            return shell('sudo kextunload -b ' + self.k.str(module))
        elif CODE().code_check(os_name,'LINUX'):
            if module is None or len(module) ==0:
                return K_ERROR
            else:
                for mm in list(module):
                   aa=shell('rmmod {0}'.format(mm))
                   if CODE().code_check(rc,K_FAIL) or CODE().code_check(rc,K_ERROR):
                      aa=shell('modprob -r {0}'.format(mm))
                      if CODE().code_check(rc,K_FAIL) or CODE().code_check(rc,K_ERROR):
                         return rc
            return rc

    def bmc():
        db={}
        tmp=rshell("ipmitool lan print")
        if tmp[0] == 0:
            bmc_lan_info=tmp[1]
            for i in bmc_lan_info.split('\n'):
                line=i.split(':')
                if len(line) == 2:
                    key=line[0].strip()
                    val=line[1].strip()
                    if key != 'Cipher Suite Priv Max' and len(key) > 0 and len(val) > 0:
                        db.update({'{0}'.format(key):'{0}'.format(val)})
        tmp=rshell("ipmitool bmc info")
        if tmp[0] == 0:
            bmc_lan_info=tmp[1]
            for i in bmc_lan_info.split('\n'):
                line=i.split(':')
                if len(line) == 2:
                    key=line[0].strip()
                    val=line[1].strip()
                    if len(key) > 0 and len(val) > 0:
                        db.update({'{0}'.format(key):'{0}'.format(val)})
        return db

    def dmidecode():
        import dmidecode
        db={}
        for i in ['bios','system','baseboard','chassis','processor','cache','connector','slot']:
           if not i in db:
               db[i]={}
               tt=dmidecode.QuerySection(i)
               a=tt.keys()
               tt=tt[a[0]]
               for j in tt['data'].keys():
                   db[i].update({j:tt['data'][j]})
        db['memory']={}
        tt=dmidecode.QuerySection('memory')
        for i in tt.keys():
            if 'Maximum Capacity' in tt[i]['data']:
                db['memory'].update({'Maximum Capacity':tt[i]['data']['Maximum Capacity']})
            if 'Number Of Devices' in tt[i]['data']:
                db['memory'].update({'Number Of Devices':tt[i]['data']['Number Of Devices']})
            if 'Locator' in tt[i]['data']:
                db['memory'][tt[i]['data']['Locator']]={}
                db['memory'][tt[i]['data']['Locator']].update({'dmi_handle':'{0}'.format(i)})
                db['memory'][tt[i]['data']['Locator']].update(tt[i]['data'])
        return db



# SQLite
class SQLite:
    """
    Handle SQLite 
    """
    def __init__(self,path=None):
       self.require_pkg=['sqlite3']
       if len(self.require_pkg) > 0:
         for ii in list(self.require_pkg):
            if METHOD().is_not_loaded_module(ii,globals()) == K_OK:
              try:
                 globals()[ii] = importlib.import_module(ii)
              except ImportError:
                 print('Please install python module : {0}'.format(ii))
       self.SQL = sqlite3

    def require_pkg(self):
       return self.require_pkg

    def setup(self,path=None):
       if path is not None:
           export_path(path)
       if CODE().code_check(imports(self.require_pkg,globals()),False):
           return K_ERROR

    def init_table(self,db_file,table_name=None,msg=None,force=0,idx='float'):
        db = self.SQL.connect(db_file,timeout=1)
        if msg is None or table_name is None:
            return db

        cur = db.cursor()
        if force == 1:
            cur.execute("""drop table if exists {tbn}"""\
                .format(tbn=table_name))
            db.commit()
        if idx is None or idx == 'auto':
           cur.execute("""create table if not exists {tbn} ( \
              idx     int     primary key autoincrement ,\
              {msg} )"""\
              .format(tbn=table_name,msg=msg))
        elif idx == 'float':
           cur.execute("""create table if not exists {tbn} ( \
              idx     float     primary key not NULL ,\
              {msg} )"""\
              .format(tbn=table_name,msg=msg))
        else:
           cur.execute("""create table if not exists {tbn} ( \
              {idx}  primary key not NULL ,\
              {msg} )"""\
              .format(idx=idx,tbn=table_name,msg=msg))
        db.commit()
        cur.close()
        return db

    def convert_put(self,dat):
        return str(dat).replace("'","[\"]")
        
    def convert_get(self,dat):
        return dat.replace("[\"]","'")

    def get_table_name(self,db):
        cur = db.cursor()
        cur.execute("SELECT name FROM sqlite_master WHERE type='table' and name != 'sqlite_sequence';")
        rc=[]
        for tb in list(cur.fetchall()):
            rc.append(tb[0])
        return rc

    def get_field_name(self,db,table_name):
        cur = db.cursor()
        try:
            cur.execute('pragma table_info('+table_name+')')
            field=[]
            for ln in list(cur.fetchall()):
                field.append(ln[1])
            return field
        except:
            return
        
    def put(self,db,table_name,path,value=None,ptype=None,new=0,check_condition=None,idx='float'):
        """ condition is None case:
                automatically find field name from table_name.
                path is '/v1/v2/v3' =>  field1="v1", field2="v2", field3="v3"
            condition have something like this "name='abc' and age='18' or age='20'":
                path will be field name.
            value is None then the field will be got ''.
        """
        if new == 0:
            num=self.find(db,table_name,path,check_condition)
            if len(num) > 0:
                return K_OK
        cur = db.cursor()
        if idx == 'float':
           idx = CMD().now(1)
        elif (idx == 'auto' or idx is None ) and ptype is not None:
           idx = 'auto'
        else:
           return K_FALSE

        if ptype is None:
            path_arr=DATA().path2list(path)
            sq_fields=self.get_field_name(db,table_name)
            sq_field_mx=len(sq_fields)
            if value is None:
                input_max=0
            else:
                input_max=1
            input_max=input_max + len(path_arr) + 1
            if sq_field_mx < input_max:
                return K_ERROR

            fields = ', '.join("'{}'".format(col) for col in list(sq_fields))
            values = str(idx)+',' + ', '.join("'{}'".format(vl) for vl in list(path_arr))
            if value is not None:
                #values=values+",'{0}'".format(self.convert_put(value))
                values=values+",'{0}'".format(value)
            for ii in range(input_max + 1, sq_field_mx + 1):
                values=values+",''"
            sql_msg = "insert into {0} ({1}) values({2})".format(table_name, fields, values)
        else:
            #sql_msg = "insert into {0} (idx,{1}) values({2},'{3}')".format(table_name, path,idx,self.convert_put(value))
            if idx == 'auto':
               sql_msg = "insert into {0} ({1}) values('{2}')".format(table_name, path,value)
            else:
               sql_msg = "insert into {0} (idx,{1}) values({2},'{3}')".format(table_name, path,idx,value)
        cur.execute(sql_msg)
        db.commit()

    def find(self,db,table_name,path=None,condition=None, order=None):
        """ if path is '/v1/v2/v3' then it will find 'field1="v1" and field2="v2" and field3="v3"' condition
            (automatically find field name from table_name.)
            if condition have something like this "name='abc' and age='18' or age='20'" then it will find that condtion only
            if both path and condition then find 'path condition' and 'condtion condtion'
            order for sort ordering
        """
        cur = db.cursor()
        rc=[]

        if path is None and condition is None:
            find_db='select idx from {tbn}'.format(tbn=table_name)
            cur.execute(find_db)
            for ii in list(cur.fetchall()):
                rc.append(ii[0])
            return rc

        find_condition=''
        sq_fields=[]
        if path is not None:
            sq_fields=self.get_field_name(db,table_name)
            path_arr=DATA().path2list(path)
            input_max=len(path_arr) - 1
            if len(sq_fields) < input_max:
                return K_ERROR
            ii=0
            for sqi in list(sq_fields)[1:]:
                find_condition=find_condition + ' {0}="{1}" '.format(sqi,path_arr[ii])
                if input_max <= ii:
                    break
                find_condition=find_condition + ' and '
                ii=ii+1

        if condition is not None:
            if find_condition == '':
                find_condition = condition
            else:
                find_condition = find_condition + ' and ' + condition

        if find_condition == '':
            return rc

        find_db='select idx from {tbn} where '.format(tbn=table_name)

        order_str=''
        if 'cnt' in sq_fields:
            order_str=' ORDER by CAST(cnt as integer)'

        if order is not None:
            if order_str == '':
                order_str=' ORDER by ' + order
            else:
                order_str=order_str + ',' + order
        if order_str != '':
            order_str = order_str + ' ASC'

        cur.execute(find_db + find_condition + order_str)
        for ii in list(cur.fetchall()):
            rc.append("%f"%(ii[0]))
        return rc


    def counting(self,db,table_name,idx): # not correct code yet.
        """ Update couting in the table_name when found cnt field in the table_name """
        sq_fields=self.get_field_name(db,table_name)
        for sqf in sq_fields:
           if sqf == 'cnt':
                #if isinstance(idx,list):
                if type(idx) is list:
                  for iidx in list(idx):
                     idx_ar=[]
                     idx_ar.append(iidx)
                     get_cnt=self.get2(db,table_name,idx_ar,field='cnt',no_count=1)[0][0]
                     if get_cnt == '':
                        get_cnt=1
                     else:
                        get_cnt=int(get_cnt) + 1
                     self.update2(db,table_name,idx,update_str='cnt={0}'.format(get_cnt))
                else:
                     idx_ar=[]
                     idx_ar.append(idx)
                     get_cnt=self.get2(db,table_name,idx_ar,field='cnt',no_count=1)[0][0]
                     if get_cnt == '':
                        get_cnt=1
                     else:
                        get_cnt=int(get_cnt) + 1
                     self.update2(db,table_name,idx,update_str='cnt={0}'.format(get_cnt))

    def update2(self,db,table_name,idx=None,update_str=None,bin_body=None):
        '''
            if field type is BLOB then can binary data to the field in SQLite
        '''
        if update_str is None or idx is None:
            return K_ERROR
        #if isinstance(idx,list):
        if type(idx) is list:
            update_db="update {tbn} set {values} where idx={idx} ".format(tbn=table_name,values=update_str,idx=idx[0])
            for iidx in list(idx)[1:]:
                update_db=update_db + " or idx={idx} ".format(tbn=table_name,idx=iidx)
        else:
            update_db="update {tbn} set {values} where idx={idx} ".format(tbn=table_name,values=update_str,idx=idx)
        cur = db.cursor()
        if bin_body is not None:
            cur.execute(update_db,bin_body)
        else:
            cur.execute(update_db)
        db.commit()


    def update(self,db,table_name,path,value=None,condition=None,multi=0,auto=1):
        """ condition is None case:
                automatically find field name from table_name.
                path is '/v1/v2/v3' =>  field1="v1" and field2="v2" and field3="v3"
                path will be finding condition
            condition have something like this "name='abc' and age='18' or age='20'":
                path will be field name.
            value is None then the field will be got ''.
        """
        num=self.find(db,table_name,path,condition)
        if num == -1:
            return K_ERROR

        if len(num) == 0:
            if auto == 1:
                self.put(db,table_name,path,value,type)
            else:
                return K_OK
        else:
            if value is None:
                value=''
            if condition is None:
                path_arr=DATA().path2list(path)
                input_max=len(path_arr)+1
                sq_fields=self.get_field_name(db,table_name)
                field_name=sq_fields[input_max]
                find_condition=''
                ii=0
                for sqi in list(sq_fields)[1:]:
                    find_condition=find_condition + " {0}='{1}' ".format(sqi,path_arr[ii])
                    if (input_max-2) <= ii:
                        break
                    find_condition=find_condition + ' and '
                    ii=ii+1
            else:
                field_name=path
                find_condition=condition

            if multi == 0:
                if len(num) > 1 :
                    return K_ERROR
                else:
                    update_db="update {tbn} set {field}='{value}' where idx={idx} ".format(tbn=table_name,field=field_name,value=value,idx=num[0])
            else:
                update_db="update {tbn} set {field}='{value}' where ".format(tbn=table_name,field=field_name,value=value)
                update_db=update_db + find_condition
            cur = db.cursor()
            cur.execute(update_db)
            db.commit()

    def get2(self,db,table_name,idx,field=None,uniq=0,no_count=0):
        if idx is None:
           return K_ERROR
        if field is None:
            find_field='*'
        else:
            find_field=field

        find_condition=' where idx={0}'. format(idx[0])

        for iidx in list(idx)[1:]:
           find_condition=find_condition + ' or idx={0}'.format(iidx)

        if uniq == 1:
           find_db='select DISTINCT {ffield} from {tbn}'.format(ffield=find_field,tbn=table_name)
        else:
           find_db='select {ffield} from {tbn}'.format(ffield=find_field,tbn=table_name)
        cur = db.cursor()
        cur.execute(find_db + find_condition)
        if no_count != 1:
           if len(idx) == 1:
              self.counting(db,table_name,idx)

        return cur.fetchall()

    def get(self,db,table_name,path,condition=None,field=None,uniq=0,order=None):
        """ condition is None case:
                automatically find field name from table_name.
                path is '/v1/v2/v3' =>  field1="v1" and field2="v2" and field3="v3"
                path will be finding condition
            condition have something like this "name='abc' and age='18' or age='20'":
                path will be field name.
            value is None then the field will be got ''.
        """
        cur = db.cursor()
        if condition is None:
            path_arr=DATA().path2list(path)
            #if path_arr is None:
            #    return
            sq_fields=self.get_field_name(db,table_name)
            if sq_fields is None or len(sq_fields) == 0:
                return
            #input_max=len(path_arr) - 2
            if len(path_arr) == 1 and len(path_arr[0]) == 0:
                input_max=0
                if field is None:
                    find_field=sq_fields[1]
                else:
                    find_field=field

                if uniq == 1:
                    find_db='select DISTINCT {ffield} from {tbn}'.format(ffield=find_field,tbn=table_name)
                else:
                    find_db='select {ffield} from {tbn}'.format(ffield=find_field,tbn=table_name)
                if order is None:
                    cur.execute(find_db)
                else:
                    cur.execute(find_db+' '+order)
                return cur.fetchall()
            else:
                input_max=len(path_arr) - 1
                if len(sq_fields) < input_max:
                    return K_ERROR
                find_condition=''
                ii=0
                for sqi in list(sq_fields)[1:]:
                    find_condition=find_condition + " {0}='{1}' ".format(sqi,path_arr[ii])
                    if input_max <= ii:
                        break
                    find_condition=find_condition + ' and '
                    ii=ii+1
                if field is None:
                    if len(sq_fields) <= ii+2 :
                        return
                    find_field=sq_fields[ii+2]
                else:
                    find_field=field
        else:
            find_condition=condition
            if field is None:
                find_field=path
            else:
                find_field=field

        if uniq == 1:
            find_db='select DISTINCT {ffield} from {tbn} where '.format(ffield=find_field,tbn=table_name)
        else:
            find_db='select {ffield} from {tbn} where '.format(ffield=find_field,tbn=table_name)
        if order is None:
            order=''

        cur.execute(find_db+' '+find_condition+' '+order)

        aa=cur.fetchall()
        return aa

    def delete(self,db,table_name,path,condition=None,multi=0):
        """ condition is None case:
                automatically find field name from table_name.
                path is '/v1/v2/v3' =>  field1="v1" and field2="v2" and field3="v3"
                path will be finding condition
            condition have something like this "name='abc' and age='18' or age='20'":
                path will be field name.
        """
        num=self.find(db,table_name,path,condition)
        if num == -1:
            return K_ERROR

        if len(num) == 0:
            return K_OK
        else:
            if condition is None:
                path_arr=DATA().path2list(path)
                input_max=len(path_arr)+1
                sq_fields=self.get_field_name(db,table_name)
                field_name=sq_fields[input_max]
                find_condition=''
                ii=0
                for sqi in list(sq_fields)[1:]:
                    find_condition=find_condition + " {0}='{1}' ".format(sqi,path_arr[ii])
                    if (input_max-2) <= ii:
                        break
                    find_condition=find_condition + ' and '
                    ii=ii+1
            else:
                field_name=path
                find_condition=condition

            if multi == 0:
                if len(num) > 1 :
                    return K_ERROR
                else:
                    update_db='delete from {tbn} where idx={idx} '.format(tbn=table_name,idx=num[0])
            else:
                update_db='delete from {tbn} where '.format(tbn=table_name)
                update_db=update_db + find_condition
            cur = db.cursor()
            cur.execute(update_db)
            rc=db.commit()
            return rc

    def csl(self,obj,tag):
        if type(obj).__name__ == 'list':
            return obj
        if obj.find(tag) >= 0:
            return obj.split(tag)
        else:
            return obj

    def get_sql(self,db,table,path):
        rc=[]
        aa=self.get(db,table,path,uniq=1)
        deep=0
        tmp=[]
        if aa is None:
            return
        for row in list(aa):
            bb=self.get(db,table,path+'/'+row[0],uniq=1)
            if bb is None or len(bb[0][0]) == 0:
                if len(aa) > 1:
                    rc.append('0')
                    rc.append(path)
                    rc.append(aa[0][0])
                else:
                    rc.append('0')
                    rc.append(path)
                    rc.append(self.csl(row[0],','))
                break
            else:
                deep=1
                tmp.append(path+'/'+row[0])
        if len(tmp) > 0:
            rc.append('1')
            rc.append(tmp)
        return rc

    def sql2dict(self,dic_name,db,table,ppth=None):
        if ppth is None:
            ppth='/'
        else:
            ppth=ppth

        kdk=self.get(db,table,ppth,uniq=1)
        for row in list(kdk):
           if ppth == '/':
               pppth='/'+row[0]
           else:
               pppth=ppth+'/'+row[0]
           rc=self.get_sql(db,table,pppth)
           if rc is None:
               DATA().dict_put(dic_name,'/'+table+ppth,self.csl(row[0],','))
           else:
               if rc[0] == '0':
                   DATA().dict_put(dic_name,'/'+table+rc[1],self.csl(rc[2],','))
               else:
                   for rr in list(rc[1]):
                      self.sql2dict(dic_name,db,table,rr)

# Daemon
class Daemon:
    """
    A generic daemon class.
    Usage: subclass the Daemon class and override the run() method
    """

    def __init__(self, pidfile, stdin='/dev/null',
                 stdout='/dev/null', stderr='/dev/null', path=None):
       self.require_pkg=[]
       self.stdin = stdin
       self.stdout = stdout
       self.stderr = stderr
       self.pidfile = pidfile
       if len(self.require_pkg) > 0:
         for ii in list(self.require_pkg):
            if METHOD().is_not_loaded_module(ii,globals()) == K_OK:
              try:
                 globals()[ii] = importlib.import_module(ii)
              except ImportError:
                 print('Please install python module : {0}'.format(ii))

    def require_pkg(self):
       return self.require_pkg

    def setup(self,path=None):
       if path is not None:
           export_path(path)
       if CODE().code_check(imports(self.require_pkg,globals()),False):
           return K_ERROR


    def daemonize(self):
        """
        do the UNIX double-fork magic, see Stevens' "Advanced
        Programming in the UNIX Environment" for details (ISBN 0201563177)
        http://www.erlenstar.demon.co.uk/unix/faq_2.html#SEC16
        """
        # Do first fork
        self.fork()

        # Decouple from parent environment
        self.dettach_env()

        # Do second fork
        self.fork()

        # Flush standart file descriptors
        sys.stdout.flush()
        sys.stderr.flush()

        #
        self.attach_stream('stdin', mode='r')
        self.attach_stream('stdout', mode='a+')
        self.attach_stream('stderr', mode='a+')

        # write pidfile
#        self.create_pidfile()

    def attach_stream(self, name, mode):
        """
        Replaces the stream with new one
        """
        stream = open(getattr(self, name), mode)
        os.dup2(stream.fileno(), getattr(sys, name).fileno())

    def dettach_env(self):
        os.chdir("/")
        os.setsid()
        os.umask(0)

    def fork(self):
        """
        Spawn the child process
        """
        try:
            pid = os.fork()
            if pid > 0:
                sys.exit(0)
        except OSError as e:
            sys.stderr.write("Fork failed: %d (%s)\n" % (e.errno, e.strerror))
            sys.exit(1)

#    def create_pidfile(self):
#        atexit.register(self.delpid)
#        pid = str(os.getpid())
#        open(self.pidfile,'w+').write("%s\n" % pid)

    def delpid(self):
        """
        Removes the pidfile on process exit
        """
        os.remove(self.pidfile)

    def start(self):
        """
        Start the daemon
        """
        # Check for a pidfile to see if the daemon already runs
        pid = self.get_pid()

        if pid:
            message = "pidfile %s already exist. Daemon already running?\n"
            sys.stderr.write(message % self.pidfile)
            sys.exit(1)

        # Start the daemon
        self.daemonize()
        self.run()

    def get_pid(self):
        """
        Returns the PID from pidfile
        """
        try:
            pf = open(self.pidfile,'r')
            pid = int(pf.read().strip())
            pf.close()
        except (IOError, TypeError):
            pid = None
        return pid

    def stop(self, silent=False):
        """
        Stop the daemon
        """
        # Get the pid from the pidfile
        pid = self.get_pid()

        if not pid:
            if not silent:
                message = "pidfile %s does not exist. Daemon not running?\n"
                sys.stderr.write(message % self.pidfile)
            return # not an error in a restart

        # Try killing the daemon process
        try:
            while True:
                os.kill(pid, signal.SIGTERM)
                time.sleep(0.1)
        except OSError as err:
            err = str(err)
            if err.find("No such process") > 0:
                if os.path.exists(self.pidfile):
                    os.remove(self.pidfile)
            else:
                sys.stdout.write(str(err))
                sys.exit(1)

    def restart(self):
        """
        Restart the daemon
        """
        self.stop(silent=True)
        self.start()

    def run(self):
        """
        example)
        from cep import Daemon
        from cep import op

        class MyDaemon(Daemon):
            def run(self):
                print('Start Daemon')
                while True:
                    // function()
                    time.sleep(1)
            //def function():

        if __name__ == "__main__":
            cmd=op()
            MyD=MyDaemon('<pid file>',stdout='<output file>',stderr='<error log file>')
            if cmd == 'start':
                MyD.start()
            elif cmd == 'stop':
                MyD.stop()
            elif cmd == 'status':
                pid=MyD.get_pid()
                if not pid:
                    print('Not run')
                else:
                    print('Run [PID=%d]'%pid)
            elif cmd == 'restart':
                MyD.restart()
            sys.exit(0)
        """
        pass

class OS:
    class OSX:
        def sms_read(self,receive_account=None,sender_account=None,self_sender_ignore=True,imessage_db="{0}/Library/Messages/chat.db".format(os.path.expanduser('~')),read_time=0, find_text=None):
          if CODE().code_check(imports('sqlite3',globals()),False):
             return K_NOT_FOUND
          imconn=sqlite3.connect(imessage_db)
          imlg=imconn.cursor()
          imessage_get="SELECT h.id, m.text, m.date FROM message m JOIN handle h ON m.handle_id=h.ROWID"
          receive_account_str=''
          sender_account_list=''
          self_sender_ignore_str=''
          block_recive_account=''
          set_mode={}
          wait_time=10 # 10min
          if receive_account != "all" and receive_account is not None:
             imessage_get=imessage_get+" and m.account='e:{0}' and h.id!='{1}'".format(receive_account,receive_account)
        
          if sender_account is not None:
             for ii in list(sender_account.split(',')):
                 if sender_account_list == '':
                    sender_account_list = "h.id='{0}'".format(ii)
                 else:
                    sender_account_list = "{0} or h.id='{1}'".format(sender_account_list,ii)
             imessage_get = imessage_get + " and ( {0} ) ".format(sender_account_list)
        
          if self_sender_ignore:
             imessage_get = imessage_get + ' and m.is_from_me=0'
          if find_text is not None:
              imessage_get = imessage_get + " and m.text LIKE '%{0}%'".format(find_text)
        
          if read_time > 0:
              imessage_get = imessage_get + " and m.date > %s" % (read_time)
          imlg.execute(imessage_get)
          imessage_fetchall=imlg.fetchall()
          imessage_last=len(imessage_fetchall)
          if imessage_last == 0:
                return
        
          return imessage_fetchall


class NET:
    def __init__(self,path=None):
       self.require_pkg=[]
       self.sql = SQLite()
       if len(self.require_pkg) > 0:
         for ii in list(self.require_pkg):
            if METHOD().is_not_loaded_module(ii,globals()) == K_OK:
              try:
                 globals()[ii] = importlib.import_module(ii)
              except ImportError:
                 print('Please install python module : {0}'.format(ii))

    def require_pkg(self):
       return self.require_pkg

    def setup(self,path=None):
       if path is not None:
           export_path(path)
       if CODE().code_check(imports(self.require_pkg,globals()),False):
           return K_ERROR

    def url(self,url,data=None,header=None):
        import urllib
        ver3=is_ver3()
        if ver3 == K_TRUE:
           import urllib.request as ulib
        else:
           import urllib2 as ulib
        if data is None:
            req=url
        else:
            data=urllib.urlencode(data)
            if header is None:
                req = ulib.Request(url,data)
            else:
                req = ulib.Request(url,data,header)
        try:
            uo = ulib.urlopen(url)
        except:
            print("can't find {0}".format(url))
            return False
        if ver3 == K_TRUE:
            size = int(uo.headers["Content-Length"])
            ctype = uo.headers["Content-Type"]
        else:
            meta = uo.info()
            size = int(meta["Content-Length"])
            ctype = meta["Content-Type"]
        return uo,size,ctype

    def valid_IPv4_format(self,ipaddress=None):
        if ipaddress is None or type(ipaddress).__name__ != 'str':
           return K_ERROR
        parts = ipaddress.split(".")
        if len(parts) != 4:
            return K_ERROR
        for item in parts:
            if not item.isdigit() or not 0 <= int(item) <= 255:
                return K_ERROR
        return K_OK

    def host(self,hostname,type='auto',db=None):
        ipaddr=[]
        if type == 'db':
            if db is None: return
            ipaddr=self.sql.get(db,'host','/{0}'.format(hostname))            
            if ipaddr is None or len(ipaddr) == 0: return
            return ipaddr[0][0]
        elif type == 'hostname':
                try:
                    return socket.gethostbyaddr(hostname)[0]
                except socket.error:
                    return 
        elif type == 'ip':
                try:
                    return socket.gethostbyname(hostname)
                except socket.error:
                    return 
        elif type == 'no_db':
                try:
                    return socket.gethostbyaddr(hostname)[0]
                except socket.error:
                    try:
                        return socket.gethostbyname(hostname)
                    except socket.error:
                        return
                except UnicodeError:
                    return
        else:
                if db is None: return
                ipaddr=self.sql.get(db,'host','/{0}'.format(hostname))            
                if ipaddr is None or len(ipaddr) == 0:
                    try:
                        return socket.gethostbyaddr(hostname)[0]
                    except socket.error:
                        try:
                            return socket.gethostbyname(hostname)
                        except socket.error:
                            return 
                    except UnicodeError:
                        return
                else:
                    return ipaddr[0][0]
            
    def check_network(self):
      """ Check network connection """
      REMOTE_SERVER = "www.google.com"
      try:
         host = socket.gethostbyname(REMOTE_SERVER)
         s = socket.create_connection((host, 80), 2)
         return K_OK
      except:
         return K_FALSE

    def get_local_ip(self):
      s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
      s.connect(("8.8.8.8", 80))
      return s.getsockname()[0]

    def get_device_ip(self,dev=None,ipv=4):
      if CODE().code_check(imports('netifaces',globals()),False):
         return K_ERROR
      if dev is None:
         return K_FALSE
      else:
         if ipv==4:
            return netifaces.ifaddresses(dev)[2][0]['addr']
         else:
            return netifaces.ifaddresses(dev)[10][0]['addr']
    
    def num2ip(self,ipnum):
       if isinstance(ipnum,int) is False:
           return
       ip1=(ipnum/(256*256*256))%256
       ip2=(ipnum/(256*256))%256
       ip3=(ipnum/256)%256
       ip4=ipnum%256
       ip_addr='%d.%d.%d.%d' % (ip1,ip2,ip3,ip4)
       return ip_addr

    def ip2num(self,ipaddr):
       if self.valid_IPv4_format(ipaddr) is True:
          iparr=ipaddr.split('.')
          return (int(iparr[3])+(int(iparr[2])*256)+(int(iparr[1])*(256*256))+(int(iparr[0])*(256*256*256)))

    def hex2mac(self,hex_val):
       mac_str=[]
       mac_str.append(":".join([hex_val[i] + hex_val[i+1] for i in range(2,14,2)]))
       return mac_str[0].upper()

    def int2mac(self,int_val):
       if isinstance(int_val,int) is False:
           return
       return hex2mac(DATA().int2hex(int_val))

    def mac2int(self,mac_val):
       return DATA().hex2int(mac_val)

    def mac2ipv6(mac_val,prefix='fe80::',dev=None):
       '''
       Convert MAC to IPv6(Prefix(FE80::/64bit) + Interface ID(convrt MAC to 64bit))
       '''
       mac_tmp=mac_val.split(":")
       ipv6bin=CEP().hex2bit(mac_tmp[0]+mac_tmp[1])
       # 7th bit flipped
       if ipv6bin[6] == '0':
          ipv6tmp=ipv6bin[:6]+'1'+ipv6bin[7:]
       else:
          ipv6tmp=ipv6bin[:6]+'0'+ipv6bin[7:]
       ipv61=DATA().bit2hex(ipv6tmp)
       ipv62=mac_tmp[2]+'FF'
       ipv63='FE'+mac_tmp[3]
       ipv64=mac_tmp[4]+mac_tmp[5]
       if prefix=='fe80::':
           if dev is not None:
               return prefix+ipv61.lower()+':'+ipv62.lower()+':'+ipv63.lower()+':'+ipv64.lower()+'%{0}'.format(dev)
       return prefix+ipv61.lower()+':'+ipv62.lower()+':'+ipv63.lower()+':'+ipv64.lower()

class HTTP:
    def __init__(self,path=None):
       self.require_pkg=['ssl','BaseHTTPServer']
       if len(self.require_pkg) > 0:
         for ii in list(self.require_pkg):
            if METHOD().is_not_loaded_module(ii,globals()) == K_OK:
              try:
                 globals()[ii] = importlib.import_module(ii)
              except ImportError:
                 print('Please install python module : {0}'.format(ii))

    def require_pkg(self):
       return self.require_pkg

    def setup(self,path=None):
       if path is not None:
           export_path(path)
       if CODE().code_check(imports(self.require_pkg,globals()),False):
           return K_ERROR

    def http2https(self,http=None,pem_file=None):
       ''' convert http daemon to https daemon'''
       if is_loaded_module('ssl',globals()) is False:
           error_exit('Plase import ssl module')
       if http is None or pem_file is None:
           error_exit('BaseHTTPServer.HTTPServer object or pem file not input')

       if self.is_file(pem_file) is False or self.is_file(pem_file+'.key') is False:
           CEP().shell(""" openssl req -x509 -newkey rsa:2048 -keyout {0}.key -out {1} -nodes -subj "/C=US/ST=TX/L=Houston/O=CEP/OU=Dev/CN=localhost/emailAddress=''" """.format(pem_file,pem_file))
       http.socket = ssl.wrap_socket(http.socket, keyfile=pem_file+'.key', certfile=pem_file,server_side=True)
       return http

class DATA:
    def __init__(self,path=None):
       self.require_pkg=[]
       if len(self.require_pkg) > 0:
          for ii in list(self.require_pkg):
             if METHOD().is_not_loaded_module(ii,globals()) == K_OK:
                try:
                   globals()[ii] = importlib.import_module(ii)
                except ImportError:
                   print('Please install python module : {0}'.format(ii))

    def require_pkg(self):
       return self.require_pkg

    def setup(self,path=None):
       if path is not None:
           export_path(path)
       if CODE().code_check(imports(self.require_pkg,globals()),False):
           return K_ERROR

    def append(self,src,**data):
       '''
        Append **data to src
        src type is not list,tuple,dict then return value will be string
        src type is string and got symbol then make src to list with symbol and add **data to src
        data['symbol'] (ex: symbol=',') will spliting symbol
        data['extract'] (ex: extract=True/False) : True: extract list or tuple to list. False: just put anything to list
        others are will input data (ex: any=whatever format)
       '''
       symbol=None
       extract=False
       if 'symbol' in data:
           symbol=data['symbol']
           data.pop('symbol')
       if 'extract' in data:
           extract=data['extract']
           data.pop('extract')
       if type(src) is str:
          try:
             src=ast.literal_eval(src)
          except:
             pass
          if type(src) is str and symbol is not None:
              src=src.split(symbol)
       
       src_type=type(src).__name__
       if src_type == 'list' or src_type == 'tuple':
          if src_type == 'list':
              tmp=src
          else:
              tmp=tuple2list(src)
       
          if len(data) > 0:
              for k in data.keys():
                  ii=data[k]
                  ii_type=type(ii)
                  if ii_type is str and symbol is not None:
                      for jj in ii.split(symbol):
                          tmp.append(jj)
                  elif ( ii_type is list or ii_type is tuple) and extract:
                      for jj in list(ii):
                          tmp.append(jj)
                  else:
                      if ii_type is list:
                          tmp.append(list(ii))
                      elif ii_type is tuple:
                          tmp.append(tuple(ii))
                      else:
                          tmp.append(ii)
          if src_type == 'tuple':
              return self.list2tuple(tmp)
          else:
              return tmp
       elif src_type == 'dict':
          if len(data) > 0:
              for k in data.keys():
                  ii=data[k]
                  if type(ii) is dict:
                      src.update(ii)
          return src
       else:
          if len(data) > 0:
              for k in data.keys():
                  add=data[k]
                  ssrc='{0}{1}'.format(ssrc,add)
          return ssrc

    def append2list(self,src,*add):
       if len(add) > 0:
           tmp=src
           for ii in list(add):
               tmp=self.append(tmp,symbol=',',a=ii,extract=True)
           return tmp
       else:
           return self.append(src,symbol=',',extract=True)

    def tuple2list(self,data):
       tmp=[]
       for i in range(0,len(data)):
           tmp.append(data[i])
       return tmp

    def list2tuple(self,data):
       tmp=()
       for i in range(0,len(data)):
           tmp=tmp + (data[i],)
       return tmp

    def append2tuple(self,src,*add):
       symbol=','
       extract=True
       tmp=self.append((),symbol=symbol,a=src)
       if len(add) > 0:
          for ii in list(add):
             tmp=self.append(tmp,symbol=symbol,a=ii,extract=extract)
       return tmp

    def path2list(self,path):
       path_str=var(path,'str')
       path_type=KP().type(path_str)
       if path_type == 'path' or path_type == 'path-data':
           path_arr=path_str.split('/')
           path_arr.pop(0)
           return path_arr
       return K_FALSE

    def root_path(self,path):
       if len(path) == 1 and path[0] == '':
           return True
       return False

    def list2path(self,path):
       if len(path) < 1:
           return
       new_path='/'
       maxp=len(path)-1
       for ii in range(maxp):
           new_path=new_path+path[ii]+'/'
       new_path=new_path+path[maxp]
       return new_path

    def dict_put(self,dic=None,path=None,value=None,force=False,safe=True):
       path_arr=self.path2list(path)
       if CODE().code_check(path_arr,False):
           return K_FALSE
       if CODE().code_check(self.root_path(path_arr),True):
           return K_FALSE
       tmp=dic
       path_num=len(path_arr)
       for ii in path_arr[:(path_num-1)]:
           if ii in tmp.keys():
              if type(tmp[ii]) == type({}):
                  dtmp=tmp[ii]
              else:
                  if tmp[ii] == None:
                      tmp[ii]={}
                      dtmp=tmp[ii]
                  else:
                      if force:
                         vtmp=tmp[ii]
                         tmp[ii]={vtmp:None}
                         dtmp=tmp[ii]
                      else:
                         return K_FALSE
           else:
              if force:
                  tmp[ii]={}
                  dtmp=tmp[ii]
              else:
                  return K_FALSE
           tmp=dtmp
       if value is None or value == '_blank_' or value == '':
          value={}
       if path_arr[path_num-1] in tmp.keys():
           if CODE().code_check(safe,True):
               ttype=type(tmp[path_arr[path_num-1]])
               if tmp[path_arr[path_num-1]] is None or ((ttype is str or ttype is list or ttype is dict) and len(tmp[path_arr[path_num-1]]) == 0):
                  tmp.update({path_arr[path_num-1]:value})
               else:
                  return K_FALSE
           else:
               tmp.update({path_arr[path_num-1]:value})
       else:
           if CODE().code_check(force,False):
               return K_FALSE
           tmp.update({path_arr[path_num-1]:value})
       return dic

    def dict2list_path(dic,path='',with_val=False):
       tmp=[]
       if type(dic) is dict:
           for k in dic.keys():
               npath=path+'/'+k
               if type(dic[k]) is dict:
                   append2list(tmp,dict2list_path(dic[k],path=npath,with_val=with_val))
               else:
                   if with_val:
                       tmp.append(npath+"=['{0}',{1}]".format(type(dic[k]).__name__,dic[k]))
                   else:
                       tmp.append(npath)
       return tmp

    def dict2symbol_path(dic,path='',with_val=False,symbol=','):
       new_path=None
       if type(dic) is dict:
           for k in dic.keys():
               path=path+'/'+k
               if type(dic[k]) is dict:
                   dict2symbol_path(dic[k],path=path,with_val=with_val,symbol=symbol)
               else:
                   if with_val:
                       if new_path is None:
                           new_path=path
                       else:
                           new_path=new_path+symbol+path
                       return new_path+"=['{0}',{1}]".format(type(dic[k]).__name__,dic[k])
                   else:
                       if new_path is None:
                           new_path=path
                       else:
                           new_path=new_path+symbol+path
       return new_path

    def dict_chk_key(self,dic,path):
        path_keys=self.path2list(path)
        if CODE().code_check(path_keys,False):
           return False
        if CODE().code_check(self.root_path(path_keys),True):
          if type(dic) is dict:
              return K_OK
          else:
              return K_FALSE
        tmp=copy.deepcopy(dic)
        for key in list(path_keys):
            if key in tmp:
               tmp=tmp[key]
            else:
               return K_FALSE
        return K_OK

    def dict_depth(self,dic,depth=0):
       level_depth={}
       try :
             is_key=dic.keys()
       except:
             return depth
       
       depth=depth+1
       for ii in list(is_key):
          level_depth[ii]=depth
          new_depth=self.dict_depth(dic[ii],level_depth[ii])
          if new_depth == 0:
             return level_depth[ii]
          else:
              level_depth[ii]=new_depth
       top=0
       for ii in list(level_depth):
           if level_depth[ii] > top:
               top=level_depth[ii]
       return top

    def dict_del(self,dic,path):
       path_keys=self.path2list(path)
       if CODE().code_check(path_keys,False):
           return False
       if CODE().code_check(self.root_path(path_keys),True):
         dic={}
       else:
         for key in list(path_keys[:-1]):
           if key in dic:
             dic=dic[key]
           else:
             return K_FALSE
         if path_keys[-1] in dic:
           dic.pop(path_keys[-1],None)
         else:
           return K_FALSE

    def dict_get(self,dic,path):
        path_keys=self.path2list(path)
        if CODE().code_check(path_keys,False):
           return False
        if CODE().code_check(self.root_path(path_keys),True):
           return dic
        for key in list(path_keys):
           if type(dic) is dict and key in dic.keys():
              dic=dic[key]
           else:
              return False
        return dic

    def get(self,data=None,form=None,symbol=None):
       new_form=copy.deepcopy(form)
       form_type=KP().type(new_form)
       data_type=KP().type(data)
       tmp=[]
       if data_type == 'type':
           print('put(<input data>)')
           return K_FALSE
       if data_type == 'str' and len(data) > 0:
           ss=False
           se=False
           for i in ['[','{','(']:
               if data.find(i) >= 0:
                   ss=True
           for i in [']','}',')']:
               if data.find(i) >= 0:
                   se=True
           if ss and se:
               try:
                   data=ast.literal_eval(data)
                   data_type=KP().type(data)
               except:
                   pass
       if form_type == 'dict':
           if data_type == 'list' or (data_type == 'str' and symbol is not None):
               if data_type == 'str':
                   data_arr=data.split(symbol)
               else:
                   data_arr=data
           else:
               data_arr=[data]
           for i in list(data_arr):
               idata_type=KP().type(i)
               if idata_type == 'path':
                   tmp.append(self.dict_get(dic=new_form,path=i))
               else:
                   return K_FALSE
       if len(tmp) == 1:
           return tmp[0]
       return tmp

    def put(self,data=None,form=None,symbol=None,force=False,safe=True,extract=False):
       '''
       data=<Input Data>
       form=<data format or existing data>
       symbol=<,-...> : split symbol to string for list or tuple
       extract=<True:False>: True: extract data(tuple or list) and put the item to form, False: put data to form
       force=<True|False> : 
         - True: create new data to dict, 
         - False: do not create data in dict
       safe=<True|False> : 
         - True: can not update to existing data(updatable data: None, ''), 
         - False: update data at existing dict
       '''
       new_form=copy.deepcopy(form)
       form_type=KP().type(new_form)
       if form_type == 'type':
           form_type=new_form.__name__
           if form_type == 'dict':
              new_form={}
           elif form_type == 'list':
              new_form=[]
           elif form_type == 'tuple':
              new_form=()
       data_type=KP().type(data)
       if data_type == 'type':
           print('put(<input data>)')
           return K_FALSE

       if data_type == 'str' and len(data) > 0:
           ss=False
           se=False
           for i in ['[','{','(']:
               if data.find(i) >= 0:
                   ss=True
           for i in [']','}',')']:
               if data.find(i) >= 0:
                   se=True
           if ss and se:
               try:
                   data=ast.literal_eval(data)
                   data_type=KP().type(data)
               except:
                   pass
       if form_type == data_type:
           if form_type == 'dict':
               new_form.update(data)
           elif form_type == 'list':
               for i in list(data):
                  new_form.append(i)
           elif form_type == 'tuple':
               new_form=self.append(new_form,symbol=symbol,a=data,extract=extract)
           else:
               new_form=data
           return new_form
       elif form_type == 'dict':
           if data_type == 'list' or (data_type == 'str' and symbol is not None):
               if data_type == 'str':
                   data_arr=data.split(symbol)
               else:
                   data_arr=data
           else:
               data_arr=[data]
           for i in list(data_arr):
               idata_type=KP().type(i)
               if idata_type == 'path-data':
                   idata_arr=i.split('=')
                   a=self.dict_put(dic=new_form,path=idata_arr[0],value=idata_arr[1],force=force,safe=safe)
                   if KP().type(a) == 'dict':
                       new_form.update(a)
               elif idata_type == 'path':
                   a=self.dict_put(dic=new_form,path=i,value=None,force=force,safe=safe)
                   if KP().type(a) == 'dict':
                       new_form.update(a)
               else:
                   return K_FALSE
           return new_form
       elif form_type == 'list' or form_type == 'tuple':
           return self.append(new_form,symbol=symbol,a=data,extract=extract)
       elif form_type == 'bool':
           if data_type == 'int':
               if data == 0:
                   return True
               return False
           elif data_type == 'str':
               return CODE().code_check(data,[True,'yes','true','ok','YES','Yes','OK','Ok','TRUE','True',K_TRUE,K_OK])
           elif data_type == 'tuple':
               return CODE().code_check(data[0],[True,'yes','true','ok','YES','Yes','OK','Ok','TRUE','True',K_TRUE,K_OK])
           elif data_type == 'dict':
               if 'rc' in data:
                   return CODE().code_check(data['rc'],[True,'yes','true','ok','YES','Yes','OK','Ok','TRUE','True',K_TRUE,K_OK])
               elif 'return' in data:
                   return CODE().code_check(data['return'],[True,'yes','true','ok','YES','Yes','OK','Ok','TRUE','True',K_TRUE,K_OK])
               elif 'rc_code' in data:
                   return CODE().code_check(data['rc_code'],[True,'yes','true','ok','YES','Yes','OK','Ok','TRUE','True',K_TRUE,K_OK])
               elif 'rccode' in data:
                   return CODE().code_check(data['rccode'],[True,'yes','true','ok','YES','Yes','OK','Ok','TRUE','True',K_TRUE,K_OK])
           elif data_type == 'list':
               if len(data) >= 1:
                   return CODE().code_check(data[0],[True,'yes','true','ok','YES','Yes','OK','Ok','TRUE','True',K_TRUE,K_OK])
       elif form_type == 'NoneType' and symbol is not None:
           if len(data) > 0 and data_type == 'str':
               return data.split(symbol)
           else:
               return data

class MATH:
    def __init__(self,path=None):
       self.require_pkg=[]
       if len(self.require_pkg) > 0:
          for ii in list(self.require_pkg):
             if METHOD().is_not_loaded_module(ii,globals()) == K_OK:
                try:
                   globals()[ii] = importlib.import_module(ii)
                except ImportError:
                   print('Please install python module : {0}'.format(ii))

    def require_pkg(self):
       return self.require_pkg

    def setup(self,path=None):
       if path is not None:
           export_path(path)
       if CODE().code_check(imports(self.require_pkg,globals()),False):
           return K_ERROR

    def get_val_in_symbol(self,aa,symbol=['(',')']):
        rc=[]
        start=""
        end=""
        tmp=""
        opentag=0
        aa_type=type(aa)
        if aa_type is int or aa_type is float:
            return ['',aa,'']
        elif aa_type is str:
            str_max=len(aa)
            for ii in range(0,str_max):
              if aa[ii] == symbol[1] and opentag == 1:
                if str_max > ii:
                   end=aa[ii+1:]
                break
              elif aa[ii] == symbol[0] and opentag == 0:
                if str_max > ii:
                  if  aa[ii+1] != symbol[0]:
                     opentag=1
                     continue
              elif opentag == 1 and aa[ii] == symbol[0]:
                start=start+symbol[0]+tmp
                tmp=""
              elif opentag == 1:
                tmp=tmp+aa[ii]
              if opentag == 0:
                start=start+aa[ii]
            rc.append(start)
            rc.append(tmp)
            rc.append(end)
            return rc

    def bc(self,aa,symbol=['(',')']):
        while True:
            if aa.count(symbol[0]) == 0:
                aa=str(eval(aa))
                break
            if aa.count(symbol[0]) == aa.count(symbol[1]) :
                rcc=self.get_val_in_symbol(aa)
                if len(rcc[0]) > 0 and rcc[0][-1].isdigit():
                    aa=rcc[0]+'*'+str(eval(rcc[1]))+rcc[2]
                else:
                    aa=rcc[0]+str(eval(rcc[1]))+rcc[2]
            else:
                print("Syntax error")
                break
        print(aa)



# Code convert
class CODE:
    def __init__(self,path=None):
       self.require_pkg=[]
       if len(self.require_pkg) > 0:
          for ii in list(self.require_pkg):
             if METOHD().is_not_loaded_module(ii,globals()) == K_OK:
                try:
                   globals()[ii] = importlib.import_module(ii)
                except ImportError:
                   print('Please install python module : {0}'.format(ii))

    def require_pkg(self):
       return self.require_pkg

    def setup(self,path=None):
       if path is not None:
           export_path(path)
       if CODE().code_check(imports(self.require_pkg,globals()),False):
           return K_ERROR

    def get_code(self,code=None):
        if code is None:
            return K_NO_INPUT
        elif type(code).__name__ == 'set':
            return next(iter(code))
        else:
            return K_ERROR

    def int2code(self,code):
        if code == -1:
            return K_ERROR
        elif code == 0:
            return K_OK
        else:
            return K_FAIL

    def code2int(self,code):
        if code == K_TRUE:
           return 1
        elif code == K_ERROR:
           return -1
        else:
           return 0

    def shell2code(self,code):
        if code==0 or code is None:
           return K_TRUE
        elif code==-1:
           return K_ERROR
        else:
           return K_FAIL

    def code2shell(self,code):
        if code == K_TRUE:
           return 0
        elif code == K_ERROR:
           return -1
        else:
           return 1

    def bool2code(self,code):
        if code:
           return K_TRUE
        else:
           return K_FAIL

    def code2bool(self,code):
        if code == K_TRUE or code == K_OK:
           return True
        else:
           return False

    def code_convert(self,code,mode='cep'):
        code_type=type(code).__name__
        if mode == 'cep' or mode == 'set':
           if code_type == 'int' or (code_type == 'str' and CEP().is_int(code)):
              return self.int2code(int(code))
           elif code_type == 'bool':
              return self.bool2code(code)
           elif code_type == 'set':
              return code
        elif mode == 'shell':
           if code_type == 'int' or (code_type == 'str' and CEP().is_int(code)):
              return self.code2shell(self.int2code(int(code)))
           elif code_type == 'bool':
              return self.code2shell(self.bool2code(code))
           elif code_type == 'set':
              return self.code2shell(code)
        elif mode == 'bool':
           if code_type == 'int' or (code_type == 'str' and CEP().is_int(code)):
              return self.code2bool(self.int2code(int(code)))
           elif code_type == 'bool':
              return code
           elif code_type == 'set':
              return self.code2bool(code)
        elif mode == 'int':
           if code_type == 'int' or (code_type == 'str' and CEP().is_int(code)):
              return int(code)
           elif code_type == 'bool':
              return self.code2int(self.bool2code(code))
           elif code_type == 'set':
              return self.code2int(code)
        return code

    def code_check(self,code,check):
        '''
        check return code between return-code and user define code.
        single code(True) check: code_check(abc,True)
          (abc same as True? if yes then True, no then False)
        multi code(1,None,False) check: code_check(abc,[1,None,False])
          (abc's return code is in [1,None,False]? if yes then True, no then False)
        string check : code_check(abc,'yes')
          (abc's return same as 'yes'? if yes then True, no then False)
        Checkable : CEP's code, Bool, Int, Shell, String, ....
        '''
        if type(code).__name__ == 'list':
            code=code[0]
        if type(check).__name__ == 'tuple':
            check=check[0]
        check_type=type(check).__name__
        if check_type == 'list':
            for icheck in list(check):
                icheck_type=type(icheck).__name__
                if self.code_convert(code,mode=icheck_type) == icheck:
                    return True
        else:
            if self.code_convert(code,mode=check_type) == check:
                return True
        return False

class DEV:
    def dmsetup_info(self,dev=''):
        rc={}
        dm_table=rshell('''dmsetup table {0} 2>/dev/null'''.format(dev))
        if dm_table[0] == 0:
            z=1
            dev_name=dev
            if dev != '':
                z=0
            for line in dm_table[1].split('\n'):
                dev_line=line.split(' ')
                if z == 1:
                    dev_name=str(dev_line[0].split(':')[0])
                if not dev_name in rc:
                    rc[dev_name]={}
                n=[]
                nc=1
                for dev_num_f in dev_line[z+3:]:
                    if ':' in dev_num_f:
                        n.append('{0}'.format(dev_num_f))
                        nc=nc+1
                # what is ? dev_line[3+nc] (i mean, what is the number after dependency string?
                rc[dev_name].update({'start':int(dev_line[z]),'end':int(dev_line[z+1]),'type':'{0}'.format(dev_line[z+2]),'dep_num':n,'what?':int(dev_line[z+2+nc])})
        dm_info=rshell('''dmsetup info {0} 2>/dev/null'''.format(dev))
        if dm_info[0] == 0:
            for line in dm_info[1].split('\n'):
                line_a=line.split(':')
                if line_a[0] == 'Name':
                    name=line_a[1].replace(' ','')
                elif line_a[0] == 'State':
                    state=line_a[1].replace(' ','')
                elif line_a[0] == 'Open count':
                    mount=line_a[1].replace(' ','')
                elif line_a[0] == 'Number of targets':
                    target=line_a[1].replace(' ','')
                elif line_a[0] == 'Major, minor':
                    own_num=line_a[1].replace(' ','').replace(',',':')
                elif line_a[0] == 'UUID':
                    uuid=line_a[1].replace(' ','')
                    rc[name].update({'mount':int(mount),'state':str(state),'target':int(target),'own_num':['{0}'.format(own_num)],'uuid':'{0}'.format(uuid)})
        for info in rc.keys():
            own_dev=self.block_dev_name(rc[info]['own_num'])
            dep_dev=self.block_dev_name(rc[info]['dep_num'])
            rc[info].update({'map':{'{0}'.format(own_dev[0]):dep_dev}})
        return rc

    def block_dev_name(self,dev_num=[]):
        rc=[]
        block_dev_path='/dev/block'
        if os.path.isdir(block_dev_path):
            for mm in list(dev_num):
                link_path='{0}/{1}'.format(block_dev_path,mm)
                if os.path.islink(link_path):
                    link_path=os.path.realpath(link_path)
                if os.path.exists(link_path):
                    if stat.S_ISBLK(os.stat(link_path).st_mode):
                        rc.append(link_path)
            return rc

    def find_dev_name(self,major=None,minor=None):
        # Not tested yet need update code here
        dpath='/dev/block'
        if major is None or minor is None:
            return
        file_path='{0}/{1}:{2}'.format(dpath,major,minor)
        if os.path.islink(file_path):
            return os.path.realpath(file_path)

    def block_dev(self,dev=''):
        # Not tested yet need update code here
        dm_table=rshell('''dmsetup table {0} 2>/dev/null'''.format(dev))
        if dm_table[0] == 0:
           if dev == '':
               for line in dm_table[1].split('\n'):
                   dev_line=line.split(' ')
                   dev_name=dev_line[0].split(':')[0]
                   dev_major_num=dev_line[4].split(':')[0]
                   dev_minor_num=dev_line[4].split(':')[1]
                   self.find_dev_name(dev_major_num,dev_minor_num)
           else:
               dev_line=line.split(' ')
               dev_major_num=dev_line[3].split(':')[0]
               dev_minor_num=dev_line[3].split(':')[1]
               self.find_dev_name(dev_major_num,dev_minor_num)

    def losetup_info(self,devs=[]):
#        los_table=rshell('''losetup 2>/dev/null''')
#        rc={}
#        for line in los_table.split('\n'):
#            if not 'NAME' in line:
#                 line_a=line.strip().split(' ')
#                 dev_name='{0}'.format(line_a[0])
#                 if not dev_name in rc:
#                     rc[dev_name]={}
#                 rc[dev_name].update({'sizelimit':int(line_a[1]),'offset':int(line_a[2]),'autoclear':int(line_a[3]),'ro':int(line_a[4]),'filename':'{0}'.format(line_a[5])})
#        return rc
        rc={}
        for dev in list(devs):
            name='/dev/{0}'.format(dev.split('/')[-1])
            block_dev='/sys/block/{0}'.format(dev.split('/')[-1])
            if os.path.isdir('{0}/loop'.format(block_dev)):
                if not name in rc:
                    rc[name]={}
                with open('{0}/size'.format(block_dev),'r') as f:
                    rc[name]['size']=f.read().replace('\n','')
                with open('{0}/dev'.format(block_dev),'r') as f:
                    rc[name]['own_num']=f.read().replace('\n','')
                with open('{0}/loop/offset'.format(block_dev),'r') as f:
                    rc[name]['offset']=f.read().replace('\n','')
                with open('{0}/loop/autoclear'.format(block_dev),'r') as f:
                    rc[name]['autoclear']=f.read().replace('\n','')
                with open('{0}/loop/backing_file'.format(block_dev),'r') as f:
                    rc[name]['filename']=f.read().replace('\n','')
                with open('{0}/loop/partscan'.format(block_dev),'r') as f:
                    rc[name]['partscan']=f.read().replace('\n','')
                with open('{0}/loop/sizelimit'.format(block_dev),'r') as f:
                    rc[name]['sizelimit']=f.read().replace('\n','')
        return rc


    def losetup(self,dev='',filename={},opt=None,auto_load=False,max_loop=64):
        if opt is None or opt == 'info' or opt == 'list':
            return self.losetup_info()
        elif opt == 'add':
            if dev == '':
                loop_dev=self.get_loop_dev(auto_load=auto_load,max_loop=max_loop)
                if not loop_dev or loop_dev is None or 'free' in loop_dev:
                    return False
                if len(loop_dev['free']) == 0:
                    print('no more free loop device')
                    return
                dev=loop_dev['free'][0]
            if len(filename) == 0:
                filename={'filename':'/tmp/test_loopsetup.file','bs':'4k','count':1024}
            if 'filename' in filename and 'bs' in filename and 'count' in filename:
                if self.make_block_file(filename):
                    los=rshell('''losetup {0} {1}'''.format(dev,filename['filename']))
                    if los[0] == 0:
                        return True
        elif dev != '' and opt == 'del':
            aa=CMD().mountpoint(dev)
            if aa[0]:
                print('it used at {0}'.format(aa[1]))
            else:
                los=rshell('''losetup -d {0} 2>/dev/null'''.format(dev))
                if los[0] == 0:
                    return True
        return False
        
    def max_loop_num(self,auto_load=False,max_loop=64):
        if not os.path.isdir('/sys/module/loop'):
            if auto_load:
                CMD().insmod('loop','max_loop={0}'.format(max_loop))
            else:
                print('Please load loop device')
                return False
        with open('/sys/module/loop/parameters/max_loop','ro') as f:
            max_loop=f.read().split('\n')[0]
        if max_loop == 0:
            return 8
        else:
            return int(max_loop)

    def get_loop_dev(self,auto_load=False,max_loop=64):
        rc={'occupy':[],'free':[]}
        max_loop_n=self.max_loop_num(auto_load=auto_load,max_loop=max_loop)
        if not max_loop_n:
            return False
        for i in range(0,max_loop_n):
            dev='/sys/block/loop{0}'.format(i)
            if os.path.isdir('{0}/loop'.format(dev)):
                rc['occupy'].append('/dev/loop{0}'.format(i))
            else:
                rc['free'].append('/dev/loop{0}'.format(i))
        return rc

    def make_block_file(self,filename={},force=False):
        if 'filename' in filename and 'bs' in filename and 'count' in filename:
            if force is False:
                if os.path.exists(filename['filename']):
                    if stat.S_ISBLK(os.stat(filename['filename']).st_mode):
                        return True
                    print('''file {0} found'''.format(filename['filename']))
                    return False
            if os.path.isdir(os.path.dirname(filename['filename'])):
                dd=rshell('''dd if=/dev/zero of={0} bs={1} count={2}'''.format(filename['filename'],filename['bs'],filename['count']))
                if dd[0] == 0:
                    return True
                else:
                    print('''{0}\n{1}'''.format(dd[1],dd[2]))
                    return False
            print('''{0} directory not found'''.format(os.path.dirname(filename['filename'])))
            return False
        print('''input example) {'filename':'<path/filename>','bs':'<block size>','count':<count>}''')
        return False

    def mkdev(self,dev=None,major=None,minor=None,total=0,dev_type='b'):
        if dev is None or not dev_type in ['b','c']:
            print('Not found dev name or wrong device type({0})'.format(dev_type))
            return False
        dev_name=dev.split('/')[-1]
        def get_minor(minor=None):
            if 'sd' in dev_name:
                mm=['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
                fmi=mm.index(dev_name[-1])
                fminor=fmi * 16
                if minor is None:
                    return fminor
                else:
                    if fminor <= int(minor) and int(minor) < (fmi + 1) * fminor:
                        return minor
            else:
                if minor is None:
                    return 0
                return int(minor)
        minor_num=get_minor(minor)
        if minor_num is None:
            print('wrong minor({0}) number for {1}'.format(minor,dev))
            return False
        if total == 0:
            total=minor+1
        else:
            total=minor+total
        def get_major():
            with open('/proc/devices','ro') as f:
                devices=f.read()
            for i in devices.split('\n'):
                j=i.strip().split(' ')
                if len(j) == 2 and j[1] in dev_name:
                    return int(j[0])
        if major is None: 
            major=get_major()
        else:
            major_num=get_major()
            if int(major) != major_num:
                print('wrong major({0}) number, it should be {1} for {2}'.format(major,major_num,dev))
                return False

        if major is not None:
            for i in range(minor_num,total):
                if dev_name == 'loop':
                    if not os.path.exists('{0}{1}'.format(dev,i)):
                        os.system('mknod {0}{3} {1} {2} {3}'.format(dev,dev_type,major,i))
                elif 'sd' in dev_name:
                    if i%16 == 0:
                        if not os.path.exists('{0}'.format(dev)):
                            os.system('mknod {0} {1} {2} {3}'.format(dev,dev_type,major,i))
                    else:
                        if not os.path.exists('{0}{1}'.format(dev,i%16)):
                            os.system('mknod {0}{4} {1} {2} {3}'.format(dev,dev_type,major,i,i%16))

class KP:
    '''
      Re-defined function names from Python default functions by Kage
    '''
    def list(self,src,r='',convert=True,symbol=',',default=False,cmd=None,data=None,find=None):
        '''
          src=<source data>
          r=<range(X:Y,X:,:Y) or index(X)>
          convert=<*True: convert string/tuple to list, False:Only list src>
          symbol=',': convert string to list symbol
          default=<True: Python default rule(output: X...Y-1), *False:(output: X...Y)>
          cmd=<append/add: append data, del/remove/pop: delete data, idx: get index for find, dict: get dict for find>
          data=<appending data to src>
        '''
        if cmd == 'append' or cmd == 'add':
            return DATA().append(src,symbol=symbol,extract=convert,a=data)
        elif cmd == 'idx' and find is not None:
            rc=[]
            j=0
            for i in src:
                i_type=type(i)
                if i_type is dict or i_type is list or i_type is tuple:
                    if find in i:
                        rc.append(j)
                else:
                    if find == i:
                        rc.append(j)
                j=j+1
            return rc
        elif cmd == 'dict' and find is not None:
            rc={}
            for i in src:
                if type(i) is dict:
                    if find in i:
                        rc.update(i[find])
            return rc
        else:
            r0=None
            r1=None
            r_max=0
            if type(r) is int:
                r0=r
            elif ':' in r:
                r_arr=r.split(':')
                if r_arr[0] != '':
                    r_max=len(r_arr)
                    r0=int(r_arr[0])
            else:
                r0=int(r)
            if r_max == 2 and r_arr[1] != '':
                if default:
                    r1=int(r_arr[1])
                else:
                    r1=int(r_arr[1])+1
            
            src_type=type(src).__name__
            if src_type == 'str' and convert:
                try:
                    src=ast.literal_eval(src)
                except:
                    src=src.split(symbol)
            elif src_type == 'tuple' and convert:
                tmp=[]
                for i in src:
                    tmp.append(i)
                src=tmp
            elif src_type == 'list':
                pass
            else:
                return False
            
            if r_max==0:
                if r0 is None:
                    if cmd == 'del' or cmd == 'remove' or cmd == 'pop':
                        return []
                    return src
                else:
                    if cmd == 'del' or cmd == 'remove' or cmd == 'pop':
                        return src.pop(r0)
                    return src[r0]
            elif r0 is not None and r1 is not None:
                if cmd == 'del' or cmd == 'remove' or cmd == 'pop':
                    a=src[:r0]
                    if (r1+1)<len(src):
                       return a+src[r1+1:]
                    return a
                return src[r0:r1]
            elif r0 is None and r1 is not None:
                if cmd == 'del' or cmd == 'remove' or cmd == 'pop':
                    if (r1+1) < len(src):
                        return src[r1+1:]
                return src[:r1]
            elif r0 is not None:
                if cmd == 'del' or cmd == 'remove' or cmd == 'pop':
                    return src[:r0]
                return src[r0:]
            return False

    def dict(self,data):
        data_type=type(data)
        if data_type is str:
            try:
                idata = ast.literal_eval(data)
                if type(idata) is dict:
                    return idata
            except:
                return False
        elif data_type is dict:
            return data
        else:
            return False

    def tuple(self,data):
        data_type=type(data)
        if data_type is tuple:
            return data
        elif data_type is str or data_type is list:
            return append2tuple((),data)
        else:
            return False

    def type(self,data=None):
       data_type=type(data).__name__
       if data_type == 'str':
           tmp_arr=data.split('=')
           if tmp_arr[0].find(',') < 0:
              tmp0_arr=tmp_arr[0].split('/')
              if len(tmp_arr) == 2:
                 if tmp0_arr > 1:
                    return 'path-data'
              elif len(tmp_arr) == 1 and len(tmp0_arr) > 1 and tmp0_arr[0] == '':
                 return 'path'
           return data_type
       return data_type
       
    #def bytes(self,obj):
    #  return var(obj,'bytes')

    def str(self,data,dcode='utf-8'):
        if type(data) is bytes:
            return '{0}'.format(data.decode(dcode))
        return '{0}'.format(data)

    def bytes(self,data,ecode='utf-8',binary=False):
        sdata=self.str(data)
        if binary:
            tmp=[]
            for b in sdata.encode(ecode):
                tmp.append(b)
            return tmp
        return sdata.encode(ecode)

    def int(self,data):
        data_type=type(data)
        if data_type is dict or data_type is list or data_type is tuple or data_type is bool:
            return False
        return int(data)

    def float(self,data):
        data_type=type(data)
        if data_type is dict or data_type is list or data_type is tuple or data_type is bool:
            return False
        return float(data)

    def bool(self,data):
        data_type=type(data)
        if data_type is int:
            if data == 0:
                return True
        elif data_type is str:
            return CODE().code_check(data,['ok','OK','Ok','True','true'])
        elif data_type is tuple or data_type is list:
            return data[0]
        elif data_type is dict:
            if 'rc' in data:
                return data['rc']
            elif 'rcode' in data:
                return data['rcode']
        elif data_type is set:
            return CODE().code_check(data,[K_OK,K_TRUE])
        else:
            return False

#################################
# Main Run
#################################
if __name__ == "__main__":
    print(_k_v_version)
